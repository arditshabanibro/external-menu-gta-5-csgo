#include <GDIPlus.au3>
#include <WindowsConstants.au3>

Global $Vector2D = DllStructCreate("struct; float X; float Y; endstruct")
Global $Vector3D = DllStructCreate("struct; float X; float Y; float Z;endstruct")
Global $BoneMatrix = DllStructCreate("struct; float junk01[3]; float X; float junk02[3]; float Y; float junk03[3]; float Z; endstruct")
Global $ViewMatrix = DllStructCreate("struct; float M11; float M12; float M13; float M14; float M21; float M22; float M23; float M24; float M31; float M32; float M33; float M34; float M41; float M42; float M43; float M44; endstruct")


;~ #include <sourcsee.au3>
Local $hGUI, $hGraphics, $hBitmap, $hBackBuffer, $hPen, $wedith = @DesktopWidth/2, $height = @DesktopHeight/2
$EntityPos = DllStructCreate("struct; float Kopf; float BeinL; float BeinR; float ArmL; float ArmR; float Bauch; endstruct")
_GDIPlus_Startup()
;~ HotKeySet("{ESC}","_Exit")


$hGUI = GUICreate("", @DesktopWidth, @DesktopHeight, 0, 0, $WS_POPUP, BitOR($WS_EX_LAYERED, $WS_EX_TOPMOST,$WS_EX_TRANSPARENT))
GUISetBkColor(0xABCDEF, $hGUI)
_WinAPI_SetLayeredWindowAttributes($hGUI, 0xABCDEF, 255)

$hPen = _GDIPlus_PenCreate(0xFFFF0000,2)
$hGraphics = _GDIPlus_GraphicsCreateFromHWND($hGUI)
$hBitmap = _GDIPlus_BitmapCreateFromGraphics(@DesktopWidth, @DesktopHeight, $hGraphics)
$hBackBuffer = _GDIPlus_ImageGetGraphicsContext($hBitmap)
_GDIPlus_GraphicsSetSmoothingMode($hBackBuffer, $GDIP_SMOOTHINGMODE_HIGHQUALITY) ;sets the graphics object rendering quality (antialiasing)
GUISetState()


    ;_GDIPlus_GraphicsDrawLine($hGraphics, $wedith-40, $height, $wedith+40, $height, $hPen)
    ;_GDIPlus_GraphicsDrawLine($hGraphics, $wedith+1, $height-40, $wedith+1, $height+40, $hPen)
    ;_GDIPlus_GraphicsDrawEllipse($hGraphics, $wedith-24, $height-24, 49, 49, $hPen)
    ;_GDIPlus_GraphicsDrawImage($hGraphics, $hBitmap, $wedith, $height)


Func GDIP()
GDIPClean()

GDIPColor()

$GRadius = @desktopHeight / 22

;~ _GDIPlus_GraphicsDrawEllipse($hBackBuffer, $w2sHitbox.X-1, $w2sHitbox.Y-1, 2, 2, $hPen)
_GDIPlus_GraphicsDrawEllipse($hBackBuffer, $w2sHitboxX-$GRadius/2, $w2sHitboxY-$GRadius/2, $GRadius, $GRadius, $hPen)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer,  $heal, $aimPointX-25, $aimPointY-50)
;~ _GDIPlus_GraphicsDrawString($hBackBuffer,  GetHealth($closestEntity), $aimPoint.X-$GRadius/2, $aimPoint.Y-$GRadius)
GDIPRender()
EndFunc

Func GDIPClean()
 _GDIPlus_GraphicsClear($hBackBuffer, 0xFF000000 + 0xABCDEF)
EndFunc

Func GDIPRender()
_GDIPlus_GraphicsDrawImage($hGraphics, $hBitmap, 0, 0)
EndFunc

$spottettemp = 0
$spottet = 1

Func GDIPColor()
   $spottet = See($closestEntity)
   if not $spottettemp = $spottet Then
   If not $spottet > 0  Then
   _GDIPlus_PenSetColor($hPen, 0xFFFF0000)
   Else
   _GDIPlus_PenSetColor($hPen, 0xFF00FF00)
   EndIf
   EndIf
   $spottettemp = $spottet
EndFunc

Func _Exit()
    _GDIPlus_GraphicsDispose($hGraphics)
    _GDIPlus_BitmapDispose($hBitmap)
    _GDIPlus_PenDispose($hPen)
	_GDIPlus_GraphicsDispose($hGraphic)
    GUIDelete($hGUI)
    Exit
EndFunc