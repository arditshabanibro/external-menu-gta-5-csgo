#pragma once

#include <conio.h> 
#include <windows.h>
#include <iostream>
#include <TlHelp32.h>
#include <vector>

#ifndef _Memory_H_
#define _Memory_H_
extern DWORD SIZE_;

DWORD GetModulBaseAddr(LPCWSTR ProcessName, LPCWSTR ModuleName);
DWORD64 GetModulBaseAddr64(LPCWSTR ProcessName, LPCWSTR ModuleName);

/*template<typename _ret_t> _ret_t ReadMemory(HANDLE phandle, DWORD64 address);

template<typename _ret_t> _ret_t WriteMemory(HANDLE phandle, DWORD64 address, _ret_t value);

template<typename _ret_t> _ret_t ReadMemoryOffsets(HANDLE phandle, DWORD64 address, std::vector<unsigned int> offsets);

template<typename _ret_t> _ret_t WriteMemoryOffsets(HANDLE phandle, DWORD64 address, _ret_t value, std::vector<unsigned int> offsets);*/

template<typename _ret_t> _ret_t ReadMemory(HANDLE phandle, DWORD64 address)
{
	_ret_t ret;
	ReadProcessMemory(phandle, (void*)address, &ret, sizeof(_ret_t), 0);
	return ret;
};

template<typename _ret_t> void WriteMemory(HANDLE phandle, DWORD64 address, _ret_t value)
{
	_ret_t ret = value;
	WriteProcessMemory(phandle, (void*)address, &ret, sizeof(_ret_t), 0);
};

template<typename _ret_t> _ret_t ReadMemoryOffsets(HANDLE phandle, DWORD64 address, std::vector<unsigned int> offsets)
{
	_ret_t ret;
	for (unsigned int i = 0; i < offsets.size(); ++i)
	{
		ReadProcessMemory(phandle, (void*)address, &address, sizeof(address), 0);
		address = address + offsets[i];
	}
	ReadProcessMemory(phandle, (void*)address, &ret, sizeof(_ret_t), 0);
	return ret;
};

template<typename _ret_t> void WriteMemoryOffsets(HANDLE phandle, DWORD64 address, _ret_t value, std::vector<unsigned int> offsets)
{
	_ret_t ret = value;
	for (unsigned int i = 0; i < offsets.size(); ++i)
	{
		ReadProcessMemory(phandle, (void*)address, &address, sizeof(address), 0);
		address = address + offsets[i];
	}
	WriteProcessMemory(phandle, (void*)address, &ret, sizeof(_ret_t), 0);
};


/*void MemoryWrite(HANDLE phandle, DWORD64 adresse, DWORD64 value, DWORD64 size);

DWORD64 MemoryRead(HANDLE phandle, DWORD64 adresse, DWORD64 size);

DWORD64 MemoryReadOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 size, std::vector<unsigned int> offsets);

void MemoryWriteOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 value, DWORD64 size, std::vector<unsigned int> offsets);*/

#endif
/*#pragma once

#include "stdafx.h"
#include <conio.h> 
#include <windows.h>
#include <iostream>
#include <TlHelp32.h>
#include <vector>
#ifndef _Memory_H_
#define _Memory_H_
extern DWORD SIZE_;
int GetModulBaseAddr(LPCWSTR ProcessName, LPCWSTR ModuleName);

DWORD64 GetModulBaseAddr64(LPCWSTR ProcessName, LPCWSTR ModuleName);

DWORD64 MemoryRead(HANDLE phandle, DWORD64 adresse, DWORD64 value);

DWORD64 MemoryRead64(HANDLE phandle, DWORD64 adresse, DWORD64 value);

DWORD64 MemoryReadOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 value, std::vector<unsigned int> offsets);

void MemoryWriteOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 value, std::vector<unsigned int> offsets);

void MemoryWrite(HANDLE phandle, DWORD64 adresse, DWORD64 value);

void MemoryWrite64(HANDLE phandle, DWORD64 adresse, DWORD64 value);

#endif
*/