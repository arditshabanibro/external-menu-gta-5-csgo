#pragma once
#include "stdafx.h"
#include <Windows.h>
#include <TlHelp32.h>
#include "Memory.h"

//External Pattern Scan
int ScanPatternEx(HANDLE hProc, int base, int len, char* sig, char* mask);
int PointerScanPatternEx(HANDLE hProc, int base, int len, char* sig, char* mask, int offset, int extra);