#include "stdafx.h"
#include "config.h"
#include <iostream> 
#include <fstream>
#include <string> 



__int64 configread1(int zeile, int intiger)
{
	int b = 0;
	std::string s;
	std::ifstream lesen("config.txt");

	if (lesen.is_open())
	{
		if (lesen.good())
		{
			for (int x = 0; x <= zeile; x++)
			{
				std::getline(lesen, s);
				//std::cout << s;	
			}
			lesen.close();
		}
		
		if(s != "")
		{
		switch (intiger)
		{
		case(0):
			b = std::stoi(s, nullptr, 0);
			break;
		case(1):
			b = std::stoi(s); //error
			break;
		default:
			break;
		}
		}

	}

	return b;
}

std::ofstream schreiben;


int configread(std::string Type, std::string filename)
{
	// std::ifstream is RAII, i.e. no need to call close
	std::ifstream cFile(filename);
	if (cFile.is_open())
	{
		std::string line;
		while (getline(cFile, line)) {
			line.erase(std::remove_if(line.begin(), line.end(), isspace),
				line.end());
			if (line[0] == '#' || line.empty())
				continue;
			auto delimiterPos = line.find("=");
			auto name = line.substr(0, delimiterPos);
			auto value = line.substr(delimiterPos + 1);
			//std::cout << name << " " << value << '\n';
			if (Type == name)
			{
				return std::stoi(value);
			}
		}

	}  
	else {
		  std::cerr << "Couldn't open config file for reading.\n";
		  configwirte();
		  configread(Type, filename);
	}  
	return 1;
}

void configwirte()
{

	std::ifstream FileTest("config.txt");
	if (!FileTest.is_open())
	{
	schreiben.open("config.txt");

	schreiben << "#Faster aim but more Risk.:\n";
	schreiben << "Intenal = 1\n";
	schreiben << "\n";

	schreiben << "#Aimbot:\n";
	schreiben << "SensitivityBase = 10\n";
	schreiben << "\n";

	schreiben << "#Legitmode:\n";
	schreiben << "AimFOV = 10\n";
	schreiben << "PosfinderONOFF = 1\n";
	schreiben << "BaseRandomAimSpeedX = 10\n";
	schreiben << "BaseRandomAimSpeedY = 10\n";
	schreiben << "RandomAimSpeedX = 20\n";
	schreiben << "RandomAimSpeedY = 20\n";
	schreiben << "AimRecoilBase = 1\n";
	schreiben << "AimRecoilRandomX = 1\n";
	schreiben << "AimRecoilRandomY = 1\n";
	schreiben << "MaxBones = 8\n";
	schreiben << "\n";

	schreiben << "#Silent Aim:\n";
	schreiben << "SilentAimONOFF = 1\n";
	schreiben << "SilentAimFOV = 1\n";
	schreiben << "\n";

	schreiben << "#VisibilityCheck:\n";
	schreiben << "VisibilityCheck = 2\n";
	schreiben << "\n";

	schreiben << "#Bhop:\n";
	schreiben << "BhopDelayBase = 0\n";
	schreiben << "BhopDelay = 0\n";
	schreiben << "\n";

	schreiben << "#Trigger Bot:\n";
	schreiben << "TriggerDelayBaseDOWN = 0\n";
	schreiben << "TriggerDelayDOWN = 0\n";
	schreiben << "TriggerDelayBaseUP = 50\n";
	schreiben << "TriggerDelayUP = 150\n";

	schreiben << "\n";

	schreiben << "#Keys:\n";
	schreiben << "BhopKey = 86\n";
	schreiben << "AimbotKey = 06\n";

	schreiben << "\n";

	schreiben << "#Window Position: \n";
	schreiben << "x = 0\n";
	schreiben << "y = 0\n";
	schreiben << "#Menu timer is on 50ms + Menu Speed(ms):" "\n";
	schreiben << "Menu Speed(ms) = 25\n";
	schreiben << "Menu Color(min 0 max 255) = 10\n";
	schreiben << "BypassActive = 1";
	schreiben.close();
	}


	std::ifstream rFileTest("rconfig.txt");
	if (!rFileTest.is_open())
	{
		schreiben.open("rconfig.txt");

		schreiben << "#Faster aim but more Risk.:\n";
		schreiben << "Intenal = 1\n";
		schreiben << "\n";

		schreiben << "#Aimbot:\n";
		schreiben << "SensitivityBase = 10\n";
		schreiben << "\n";

		schreiben << "#Legitmode:\n";
		schreiben << "AimFOV = 0\n";
		schreiben << "PosfinderONOFF = 0\n";
		schreiben << "BaseRandomAimSpeedX = 5\n";
		schreiben << "BaseRandomAimSpeedY = 5\n";
		schreiben << "RandomAimSpeedX = 1\n";
		schreiben << "RandomAimSpeedY = 1\n";
		schreiben << "AimRecoilBase = 1\n";
		schreiben << "AimRecoilRandomX = 1\n";
		schreiben << "AimRecoilRandomY = 1\n";
		schreiben << "MaxBones = 8\n";
		schreiben << "\n";

		schreiben << "#Silent Aim:\n";
		schreiben << "SilentAimONOFF = 1\n";
		schreiben << "SilentAimFOV = 360\n";
		schreiben << "\n";

		schreiben << "#VisibilityCheck:\n";
		schreiben << "VisibilityCheck = 2\n";
		schreiben << "\n";

		schreiben << "#Bhop:\n";
		schreiben << "BhopDelayBase = 1\n";
		schreiben << "BhopDelay = 1\n";
		schreiben << "\n";

		schreiben << "#Trigger Bot:\n";
		schreiben << "TriggerDelayBaseDOWN = 0\n";
		schreiben << "TriggerDelayDOWN = 20\n";
		schreiben << "TriggerDelayBaseUP = 50\n";
		schreiben << "TriggerDelayUP = 150\n";

		schreiben << "\n";

		schreiben << "#Keys:\n";
		schreiben << "BhopKey = 86\n";
		schreiben << "AimbotKey = 06\n";

		schreiben << "\n";

		schreiben << "#Window Position: \n";
		schreiben << "x = 0\n";
		schreiben << "y = 0\n";
		schreiben << "#Menu timer is on 50ms + Menu Speed(ms):" "\n";
		schreiben << "Menu Speed(ms) = 25\n";
		schreiben << "Menu Color(min 0 max 255) = 10\n";
		schreiben << "BypassActive = 1";
		schreiben.close();
	}
}