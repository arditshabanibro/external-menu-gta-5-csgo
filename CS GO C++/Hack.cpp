//#pragma once
//https://www.unknowncheats.me/forum/counterstrike-global-offensive/148654-bsp-parsing.html
#include "stdafx.h"
#include "patternscan.h"
#include "Memory.h"
#include "Menu.h"
#include "Hack.h"
#include "config.h"
#include <windows.h>
#include <iostream>
#include <vector>
#include <math.h>
#include "json.hpp"
#include "signatures.hpp"
#include "include/valve-bsp-parser/bsp_parser.hpp"
using namespace hazedumper;
using json = nlohmann::json;
//#include "BSP.h"

bool internalexternal = configread("Intenal","config.txt");
int SensitivityBase = configread("SensitivityBase", "config.txt");
int BaseRandomAimSpeedX = configread("BaseRandomAimSpeedX", "config.txt");
int BaseRandomAimSpeedY = configread("BaseRandomAimSpeedY", "config.txt");
int RandomAimSpeedX = configread("RandomAimSpeedX", "config.txt");
int RandomAimSpeedY = configread("RandomAimSpeedY", "config.txt");
int AimRecoilBase = configread("AimRecoilBase", "config.txt");
int AimRecoilRandomX = configread("AimRecoilRandomX", "config.txt");
int AimRecoilRandomY = configread("TriggerDelayBaseDOWN","config.txt");
float SilentAimFOV = configread("SilentAimFOV","config.txt");
float AimFOV = configread("AimFOV","config.txt");
BYTE PosfinderONOFF = configread("PosfinderONOFF","config.txt");
int TriggerDelayDOWN = configread("TriggerDelayDOWN","config.txt");
int TriggerDelayBaseDOWN = configread("TriggerDelayBaseUP","config.txt");
int TriggerDelayBaseUP = configread("TriggerDelayBaseUP","config.txt");
int TriggerDelayUP = configread("TriggerDelayUP","config.txt");
int BhopDelayBase = configread("BhopDelayBase","config.txt");
int BhopDelay = configread("BhopDelay","config.txt");
int BhopKey = configread("BhopKey","config.txt");
int AimbotKey = configread("AimbotKey","config.txt");
int VisibilityCheckONOFF = configread("VisibilityCheck","config.txt");
int MaxBones = configread("MaxBones","config.txt");
int Funktion = 0;

bool TiggerbotONOFF = 0;
bool AimbotONOFF = 0;
bool AimbotRageONOFF = 0;
bool WallHackONOFF = 0;
bool BHopONOFF = 0;
bool SilentAntiAimONOFF = 0;
bool FakeLagONOFF = 0;

DWORD Client;
DWORD Engine;
int Base = 0;
HANDLE phandle;

rn::bsp_parser BSP;
std::string lastmap;

//https://github.com/frk1/hazedumper
//https://github.com/Y3t1y3t/CSGO-Dumper/blob/master/Dumper/src/OffsetManager/OffsetManager.cpp

/*int dwClientState = 0;
int dwClientState_GetLocalPlayer = 0;
int dwClientState_IsHLTV = 0;
int dwClientState_Map = 0;
int dwClientState_MapDirectory = 0;
int dwClientState_MaxPlayer = 0;
int dwClientState_PlayerInfo = 0;
int dwClientState_State = 0;
int dwClientState_ViewAngles = 0;
int clientstate_delta_ticks = 0;
int clientstate_last_outgoing_command = 0;
int clientstate_choked_commands = 0;
int clientstate_net_channel = 0;
int dwEntityList = 0;
int dwForceAttack = 0;
int dwForceAttack2 = 0;
int dwForceBackward = 0;
int dwForceForward = 0;
int dwForceJump = 0;
int dwForceLeft = 0;
int dwForceRight = 0;
int dwGameDir = 0;
int dwGameRulesProxy = 0;
int dwGetAllClasses = 0;
int dwGlobalVars = 0;
int dwGlowObjectManager = 0;
int dwInput = 0;
int dwInterfaceLinkList = 0;
int dwLocalPlayer = 0;
int dwMouseEnable = 0;
int dwMouseEnablePtr = 0;
int dwPlayerResource = 0;
int dwRadarBase = 0;
int dwSensitivity = 0;
int dwSensitivityPtr = 0;
int dwSetClanTag = 0;
int dwViewMatrix = 0;
int dwWeaponTable = 0;
int dwWeaponTableIndex = 0;
int dwYawPtr = 0;
int dwZoomSensitivityRatioPtr = 0;
int dwbSendPackets = 0;
int dwppDirect3DDevice9 = 0;
int m_pStudioHdr = 0;
int m_yawClassPtr = 0;
int m_pitchClassPtr = 0;
int interface_engine_cvar = 0;
int convar_name_hash_table = 0;
int m_bDormant = 0;
int model_ambient_min = 0;
int set_abs_angles = 0;
int set_abs_origin = 0;
int is_c4_owner = 0;
int force_update_spectator_glow = 0;
int anim_overlays = 0;
int m_flSpawnTime = 0;
int find_hud_element = 0;

int m_iGlowIndex = 0x10488;
int PunchAngleOffset = 0x303C;
int BoneMatrixOffset = 0x26A8;
int m_bSpotted = 0x93D;
int m_bSpottedByMask = 0x980;
int TeamOffset = 0xF4;
int HealthOffset = 0x100;
int DormantOffset = 0xED;
int m_fFlags = 0x104;
//int dwClientState_Map = 0x28C;
//int dwClientState_MapDirectory = 0x188;
//int clientstate_choked_commands = 0x4D30;
//int clientstate_last_outgoing_command = 0x4D2C;
//int clientstate_delta_ticks = 0x174;
int GameDirectory = 0x628700;
int m_vecOrigin = 0x138;
int m_vecVelocity = 0x114;
int m_vecViewOffset = 0x108;
//int dwClientState_ViewAngles = 0x4D90;
int m_iShotsFired = 0x103E0;
int m_nTickBase = 0x3440;
int m_flNextPrimaryAttack = 0x3248;
int m_hActiveWeapon = 0x2F08;
int m_iItemDefinitionIndex = 0x2FBA;;
int m_iCrosshairId = 0x11838;
int m_fAccuracyPenalty = 0x3340;
int index = 0x64;

int dwLocalPlayer = 0xDB35EC;
int dwEntityList = 0x4DCEB7C;
int dwViewMatrix = 0x4DC0494;
int dwForceAttack = 0x31FF054;
int dwGlobalVars = 0x58BCC8;
int dwSensitivity = 0xDB9194;
int dwSensitivityPtr = 0xDB9168;
int dwClientState = 0x58BFC4;
int dwGlowObjectManager = 0x5316E98;
int dwInput = 0x5220150;
int dwbSendPackets = 0xD93D2;
int force_update_spectator_glow = 0x3BA6CA;

int m_iGlowIndex = 0x10488;
int PunchAngleOffset = 0x303C;
int BoneMatrixOffset = 0x26A8;
int m_bSpotted = 0x93D;
int m_bSpottedByMask = 0x980;
int TeamOffset = 0xF4;
int HealthOffset = 0x100;
int DormantOffset = 0xED;
int m_fFlags = 0x104;
int dwClientState_Map = 652;
int dwClientState_MapDirectory = 392;
int clientstate_choked_commands = 0x4D30;
int clientstate_last_outgoing_command = 0x4D2C;
int clientstate_delta_ticks = 0x174;
int GameDirectory = 0x62A880;
int m_vecOrigin = 0x138;
int m_vecVelocity = 0x114;
int m_vecViewOffset = 0x108;
int dwClientState_ViewAngles = 0x4D90;
int m_iShotsFired = 0x103E0;
int m_nTickBase = 0x3440;
int m_flNextPrimaryAttack = 0x3248;
int m_hActiveWeapon = 0x2F08;
int m_iItemDefinitionIndex = 0x2FBA;;
int m_iCrosshairId = 0x11838;
int m_fAccuracyPenalty = 0x3340;
int index = 0x64;*/

float desktopWidth = GetSystemMetrics(SM_CXSCREEN);
float desktopHeight = GetSystemMetrics(SM_CYSCREEN);

struct Vector2D
{
	float X;
	float Y;
};
Vector2D Vec2D;

struct Vector3D
{
	float X;
	float Y;
	float Z;
};
Vector3D Vec3D;

struct BM
{
	float junk01[3];
	float X;
	float junk02[3];
	float Y;
	float junk03[3];
	float Z;
};
BM BoneMatrix;
struct VM
{
	float M11;
	float M12;
	float M13;
	float M14;
	float M21;
	float M22;
	float M23;
	float M24;
	float M31;
	float M32;
	float M33;
	float M34;
	float M41;
	float M42;
	float M43;
	float M44;
};
VM ViewMatrix;

// Memory Read Poniter
void SetSendPackets(bool set)
{
	WriteMemory<BYTE>(phandle, Engine + signatures::dwbSendPackets, set);
}

BYTE GetSendPackets()
{
	return ReadMemory<BYTE>(phandle, Engine + signatures::dwbSendPackets);
}

/*struct Input_t
{
	char	  pad_0x00[0x0C];             // 0x00
	bool      m_bTrackIRAvailable;          // 0x04
	bool      m_bMouseInitialized;          // 0x05
	bool      m_bMouseActive;               // 0x06
	bool      m_bJoystickAdvancedInit;      // 0x07
	uint8_t   Unk1[44];                     // 0x08
	uintptr_t m_pKeys;                      // 0x34
	uint8_t   Unk2[100];                    // 0x38
	bool      m_bCameraInterceptingMouse;   // 0x9C
	bool      m_bCameraInThirdPerson;       // 0x9D
	bool      m_bCameraMovingWithMouse;     // 0x9E
	Vector3D m_vecCameraOffset;            // 0xA0
	bool      m_bCameraDistanceMove;        // 0xAC
	int32_t   m_nCameraOldX;                // 0xB0
	int32_t   m_nCameraOldY;                // 0xB4
	int32_t   m_nCameraX;                   // 0xB8
	int32_t   m_nCameraY;                   // 0xBC
	bool      m_bCameraIsOrthographic;      // 0xC0
	Vector3D m_vecPreviousViewAngles;      // 0xC4
	Vector3D m_vecPreviousViewAnglesTilt;  // 0xD0
	float     m_flLastForwardMove;          // 0xDC
	int32_t   m_nClearInputState;           // 0xE0
	uint8_t   Unk3[0x8];                    // 0xE4
	uintptr_t m_pCommands;                  // 0xEC
	uintptr_t m_pVerifiedCommands;          // 0xF0
};

struct UserCmd_t
{
	uintptr_t pVft;                // 0x00
	int32_t   m_iCmdNumber;        // 0x04
	int32_t   m_iTickCount;        // 0x08
	Vector3D  m_vecViewAngles;     // 0x0C
	Vector3D  m_vecAimDirection;   // 0x18
	float     m_flForwardmove;     // 0x24
	float     m_flSidemove;        // 0x28
	float     m_flUpmove;          // 0x2C
	int32_t   m_iButtons;          // 0x30
	uint8_t   m_bImpulse;          // 0x34
	uint8_t   Pad1[3];
	int32_t   m_iWeaponSelect;     // 0x38
	int32_t   m_iWeaponSubtype;    // 0x3C
	int32_t   m_iRandomSeed;       // 0x40
	int16_t   m_siMouseDx;         // 0x44
	int16_t   m_siMouseDy;         // 0x46
	bool      m_bHasBeenPredicted; // 0x48
	uint8_t   Pad2[27];
}; // size is 100 or 0x64

struct VerifiedUserCmd_t
{
	UserCmd_t m_Command;
	uint32_t  m_Crc;
};*/




/*struct Input_t
{
	char	  pad_0x00[0x0C];             // 0x00
	bool      m_bTrackIRAvailable;          // 0x04
	bool      m_bMouseInitialized;          // 0x05
	bool      m_bMouseActive;               // 0x06
	bool      m_bJoystickAdvancedInit;      // 0x07
	char   Unk1[44];                   // 0x08
	char* m_pKeys;                      // 0x34
	char   Unk2[100];                  // 0x38
	int32_t             m_nCamCommand; //0x0070
	char                pad_0074[76]; //0x0074
	bool      m_bCameraInterceptingMouse;   // 0x9C
	bool      m_bCameraInThirdPerson;       // 0x9D
	bool      m_bCameraMovingWithMouse;     // 0x9E
	char                pad_00C3[1]; //0x00C3
	Vector3D	  m_vecCameraOffset;            // 0xA0
	char                pad_00C8[8]; //0x00C8
	bool      m_bCameraDistanceMove;        // 0xAC
	char                pad_00D1[19]; //0x00D1
	bool      m_bCameraIsOrthographic;      // 0xC0
	bool      m_CameraIsThirdPersonOverview; //0x00E5
	char                pad_00E6[2]; //0x00E6
	Vector3D	  m_vecPreviousViewAngles;      // 0xC4
	Vector3D	  m_vecPreviousViewAnglesTilt;  // 0xD0
	char   Unk3[0x8];                  // 0xE4
	float     m_flLastForwardMove;          // 0xDC
	int32_t   m_nClearInputState;           // 0xE0
	uintptr_t m_pCommands;                  // 0xEC
	uintptr_t m_pVerifiedCommands;          // 0xF0
};*/

/*struct Input_t
{
	char                pad_0000[12 - 4]; //0x0000
	bool                m_fTrackIRAvailable; //0x000C
	bool                m_fMouseInitialized; //0x000D
	bool                m_fMouseActive; //0x000E
	bool                m_fJoystickAdvancedInit; //0x000F
	char                pad_0010[44]; //0x0010
	char* m_pKeys; //0x003C
	char                pad_0040[48]; //0x0040
	int32_t             m_nCamCommand; //0x0070
	char                pad_0074[76]; //0x0074
	bool                m_fCameraInterceptingMouse; //0x00C0
	bool                m_bCameraInThirdPerson; //0x00C1
	bool                m_fCameraMovingWithMouse; //0x00C2
	char                pad_00C3[1]; //0x00C3
	Vector3D              m_vecCameraOffset; //0x00C4
	char                pad_00C8[8]; //0x00C8
	bool                m_fCameraDistanceMove; //0x00D0
	char                pad_00D1[19]; //0x00D1
	bool                m_CameraIsOrthographic; //0x00E4
	bool                m_CameraIsThirdPersonOverview; //0x00E5
	char                pad_00E6[2]; //0x00E6
	Vector3D* m_angPreviousViewAngles; //0x00E8
	Vector3D* m_angPreviousViewAnglesTilt; //0x00EC
	char                pad_00F0[16]; //0x00F0
	float               m_flLastForwardMove; //0x0100
	int32_t             m_nClearInputState; //0x0104
	uintptr_t m_pCommands; //0x0108
	uintptr_t m_pVerifiedCommands; //0x010C
};*/

struct Input_t
{
	std::byte 			pad0[0xC]; // 0x0  			
	bool 				bTrackIRVerf�gbar; // 0xC		
	bool 				bMouseInitialized; // 0xD		
	bool 				bMouseActive; // 0xE	
	std::byte 			pad1[0x9A]; // 0xF  	
	bool m_fCameraInThirdPerson;        //0x00AD
	std::byte 			pad2[0x2]; // 0xAA  
	Vector3D m_vecCameraOffset;
	std::byte 			pad3[0x38]; // 0xB8
	uintptr_t m_pCommands;                // 0xF0
	uintptr_t m_pVerifiedCommands; // 0xF4
};

struct UserCmd_t
{
	uintptr_t pVft;                // 0x00
	int32_t   m_iCmdNumber;        // 0x04
	int32_t   m_iTickCount;        // 0x08
	Vector3D  m_vecViewAngles;     // 0x0C
	Vector3D  m_vecAimDirection;   // 0x18
	float     m_flForwardmove;     // 0x24
	float     m_flSidemove;        // 0x28
	float     m_flUpmove;          // 0x2C
	int32_t   m_iButtons;          // 0x30
	uint8_t   m_bImpulse;          // 0x34
	int32_t   m_iWeaponSelect;     // 0x38
	int32_t   m_iWeaponSubtype;    // 0x3C
	int32_t   m_iRandomSeed;       // 0x40
	int16_t   m_siMouseDx;         // 0x44
	int16_t   m_siMouseDy;         // 0x46
	bool      m_bHasBeenPredicted; // 0x48
	uint8_t   Pad2[27];
}; //0x64

struct VerifiedUserCmd_t
{
	UserCmd_t m_Command;
	uint32_t  m_Crc;
};


enum HitboxList
{
	HITBOX_PELVIS,
	HITBOX_L_THIGH,
	HITBOX_L_CALF,
	HITBOX_L_FOOT,
	HITBOX_R_THIGH,
	HITBOX_R_CALF,
	HITBOX_R_FOOT,
	HITBOX_SPINE1,
	HITBOX_SPINE2,
	HITBOX_SPINE3,
	HITBOX_NECK,
	HITBOX_HEAD,
	HITBOX_L_UPPERARM,
	HITBOX_L_FOREARM,
	HITBOX_L_HAND,
	HITBOX_R_UPPERARM,
	HITBOX_R_FOREARM,
	HITBOX_R_HAND,
	HITBOX_L_CLAVICLE,
	HITBOX_R_CLAVICLE,
	HITBOX_HELMET,
	HITBOX_SPINE4,
	HITBOX_MAX,
};

enum BoneList
{
	BONE_PELVIS,
	BONE_SPINE1,
	BONE_SPINE2,
	BONE_SPINE3,
	BONE_SPINE4,
	BONE_NECK,
	BONE_L_CLAVICLE,
	BONE_L_UPPER_ARM,
	BONE_L_FOREARM,
	BONE_L_HAND,
	BONE_HEAD,
	BONE_FORWARD,
	BONE_R_CLAVICLE,
	BONE_R_UPPER_ARM,
	BONE_R_FOREARM,
	BONE_R_HAND,
	BONE_WEAPON,
	BONE_WEAPON_SLIDE,
	BONE_WEAPON_R_HAND,
	BONE_WEAPON_L_HAND,
	BONE_WEAPON_CLIP1,
	BONE_WEAPON_CLIP2,
	BONE_SILENCER,
	BONE_R_THIGH,
	BONE_R_CALF,
	BONE_R_FOOT,
	BONE_L_THIGH,
	BONE_L_CALF,
	BONE_L_FOOT,
	BONE_L_WEAPON_HAND,
	BONE_R_WEAPON_HAND,
	BONE_L_FORETWIST,
	BONE_L_CALFTWIST,
	BONE_R_CALFTWIST,
	BONE_L_THIGHTWIST,
	BONE_R_THIGHTWIST,
	BONE_L_UPARMTWIST,
	BONE_R_UPARMTWIST,
	BONE_R_FORETWIST,
	BONE_R_TOE,
	BONE_L_TOE,
	BONE_R_FINGER01,
	BONE_R_FINGER02,
	BONE_R_FINGER03,
	BONE_R_FINGER04,
	BONE_R_FINGER05,
	BONE_R_FINGER06,
	BONE_R_FINGER07,
	BONE_R_FINGER08,
	BONE_R_FINGER09,
	BONE_R_FINGER10,
	BONE_R_FINGER11,
	BONE_R_FINGER12,
	BONE_R_FINGER13,
	BONE_R_FINGER14,
	BONE_L_FINGER01,
	BONE_L_FINGER02,
	BONE_L_FINGER03,
	BONE_L_FINGER04,
	BONE_L_FINGER05,
	BONE_L_FINGER06,
	BONE_L_FINGER07,
	BONE_L_FINGER08,
	BONE_L_FINGER09,
	BONE_L_FINGER10,
	BONE_L_FINGER11,
	BONE_L_FINGER12,
	BONE_L_FINGER13,
	BONE_L_FINGER14,
	BONE_L_FINGER15,
	BONE_R_FINGER15,
	BONE_MAX
};

struct Hitbox_t
{
	int       iBone;
	Vector3D    vMin;
	Vector3D    vMax;
	void Setup(int bone, Vector3D min, Vector3D max)
	{
		iBone = bone;
		vMin = min;
		vMax = max;
	}
};

Hitbox_t    Hitbox[22];

/*Input_t Input()
{
	//uint32_t signatures::dwInput = 
	//	return ReadMemory<uint32_t>(phandle, Client + signatures::dwInput);
	return ReadMemory<Input_t>(phandle, Client + signatures::dwInput + 0xF4);
	//return ReadMemory<uint32_t>(phandle, signatures::dwInput + 0xEC);
}*/

int InputCMD()
{
	//uint32_t signatures::dwInput = 
	//	return ReadMemory<uint32_t>(phandle, Client + signatures::dwInput);
	return ReadMemory<int>(phandle, Client + signatures::dwInput + 0xF0);
	//return ReadMemory<uint32_t>(phandle, signatures::dwInput + 0xEC);
}

int InputVer()
{
	//uint32_t signatures::dwInput = 
	//	return ReadMemory<uint32_t>(phandle, Client + signatures::dwInput);
	return ReadMemory<int>(phandle, Client + signatures::dwInput + 0xF4);
	//return ReadMemory<uint32_t>(phandle, signatures::dwInput + 0xEC);
}

uint32_t  GetSensitivity()
{
	return ReadMemory<uint32_t>(phandle, Client + signatures::dwSensitivity);
}

uint32_t  GetSensitivityPtr()
{
	return ReadMemory<uint32_t>(phandle, Client + signatures::dwSensitivityPtr);
}

float GetSensitivityVal()
{
	uint32_t sensitivity = GetSensitivity() ^ GetSensitivityPtr();
	return *reinterpret_cast<float*>(&sensitivity);
}

void SetSensitivityVal(float setsensitivity)
{
	uint32_t sensitivity = *reinterpret_cast<uint32_t*>(&setsensitivity) ^ GetSensitivityPtr();

	WriteMemory<uint32_t>(phandle, Client + signatures::dwSensitivity, sensitivity);
}

int GetdwLocalPlayer()
{
	return ReadMemory<DWORD>(phandle, Client + signatures::dwLocalPlayer);
}

int ClientState()
{
	return ReadMemory<DWORD>(phandle, Engine + signatures::dwClientState);
}

void ForceAttack(int a)
{
	WriteMemory<DWORD>(phandle, Client + signatures::dwForceAttack, a);
}

int GlowObjectManager()
{
	return ReadMemory<DWORD>(phandle, Client + signatures::dwGlowObjectManager);
}

float GlobalVars()
{
	//return ReadMemory<int>(phandle, Engine + signatures::dwClientState + 0x20);
	return ReadMemory<int>(phandle, Engine + signatures::dwGlobalVars + 0x8);
}

int GetWeapon()
{
	int ActiveWeapon = ReadMemory<DWORD>(phandle, GetdwLocalPlayer() + netvars::m_hActiveWeapon) & 0xFFF;
	return ReadMemory<DWORD>(phandle, Client + signatures::dwEntityList + (((ActiveWeapon)-0x1) * 0x10));
}

int GetWeaponID()
{
	return ReadMemory<int>(phandle, GetWeapon() + netvars::m_iItemDefinitionIndex);
}

float Getm_flNextPrimaryAttack()
{
	return ReadMemory<float>(phandle, GetWeapon() + netvars::m_flNextPrimaryAttack);
}

bool IsAbleToShoot()
{
	int ServerTime = ReadMemory<int>(phandle, GetdwLocalPlayer() + netvars::m_nTickBase);
	return(!(Getm_flNextPrimaryAttack() > ServerTime * GlobalVars()));
}

float IsAccuracyPenalty()
{
	return ReadMemory<float>(phandle, GetWeapon() + netvars::m_fAccuracyPenalty);
}

enum CSWeaponType
{
	WEAPONTYPE_KNIFE = 0,
	WEAPONTYPE_PISTOL,
	WEAPONTYPE_SUBMACHINEGUN,
	WEAPONTYPE_RIFLE,
	WEAPONTYPE_SHOTGUN,
	WEAPONTYPE_SNIPER_RIFLE,
	WEAPONTYPE_MACHINEGUN,
	WEAPONTYPE_C4,
	WEAPONTYPE_GRENADE,
	WEAPONTYPE_UNKNOWN
};

enum WeaponIndex {
	WEAPON_DEAGLE = 1,
	WEAPON_ELITE = 2,
	WEAPON_FIVESEVEN = 3,
	WEAPON_GLOCK = 4,
	WEAPON_AK47 = 7,
	WEAPON_AUG = 8,
	WEAPON_AWP = 9,
	WEAPON_FAMAS = 10,
	WEAPON_G3SG1 = 11,
	WEAPON_GALILAR = 13,
	WEAPON_M249 = 14,
	WEAPON_M4A1 = 16,
	WEAPON_MAC10 = 17,
	WEAPON_P90 = 19,
	WEAPON_UMP45 = 24,
	WEAPON_XM1014 = 25,
	WEAPON_BIZON = 26,
	WEAPON_MAG7 = 27,
	WEAPON_NEGEV = 28,
	WEAPON_SAWEDOFF = 29,
	WEAPON_TEC9 = 30,
	WEAPON_TASER = 31,
	WEAPON_HKP2000 = 32,
	WEAPON_MP7 = 33,
	WEAPON_MP9 = 34,
	WEAPON_NOVA = 35,
	WEAPON_P250 = 36,
	WEAPON_SCAR20 = 38,
	WEAPON_SG556 = 39,
	WEAPON_SSG08 = 40,
	WEAPON_KNIFE = 42,
	WEAPON_FLASHBANG = 43,
	WEAPON_HEGRENADE = 44,
	WEAPON_SMOKEGRENADE = 45,
	WEAPON_MOLOTOV = 46,
	WEAPON_DECOY = 47,
	WEAPON_INCGRENADE = 48,
	WEAPON_C4 = 49,
	WEAPON_KNIFE_T = 59,
	WEAPON_M4A1_SILENCER = 60,
	WEAPON_USP_SILENCER = 61,
	WEAPON_CZ75A = 63,
	WEAPON_REVOLVER = 64,
	WEAPON_KNIFE_BAYONET = 500,
	WEAPON_KNIFE_FLIP = 505,
	WEAPON_KNIFE_GUT = 506,
	WEAPON_KNIFE_KARAMBIT = 507,
	WEAPON_KNIFE_M9_BAYONET = 508,
	WEAPON_KNIFE_TACTICAL = 509,
	WEAPON_KNIFE_FALCHION = 512,
	WEAPON_KNIFE_SURVIVAL_BOWIE = 514,
	WEAPON_KNIFE_BUTTERFLY = 515,
	WEAPON_KNIFE_PUSH = 516
};
const char* GetWeaponName()
{
//	WeaponIndex WeaponIndex;
//	WeaponIndex::WEAPON_XM1014;
//	return = WeaponIndex::WEAPON_KNIFE_PUSH;
	char* Name;
	switch (GetWeaponID())  {
	case WEAPON_DEAGLE:
		return ("Deagle");
	case WEAPON_ELITE:
		return ("Dual Berettas");
	case WEAPON_FIVESEVEN:
		return ("Five-SeveN");
	case WEAPON_GLOCK:
		return ("Glock-18");
	case WEAPON_AK47:
		return ("AK-47");
	case WEAPON_AUG:
		return ("AUG");
	case WEAPON_REVOLVER:
		return ("Revolver");
	case WEAPON_AWP:
		return ("AWP");
	case WEAPON_FAMAS:
		return ("FAMAS");
	case WEAPON_G3SG1:
		return ("G3SG1");
	case WEAPON_GALILAR:
		return ("Galil");
	case WEAPON_M249:
		return ("M249");
	case WEAPON_M4A1:
		return ("M4A1");
	case WEAPON_MAC10:
		return ("MAC-10");
	case WEAPON_P90:
		return ("P90");
	case WEAPON_UMP45:
		return ("UMP-45");
	case WEAPON_XM1014:
		return ("XM1014");
	case WEAPON_BIZON:
		return ("PP-Bizon");
	case WEAPON_MAG7:
		return ("MAG-7");
	case WEAPON_NEGEV:
		return ("Negev");
	case WEAPON_SAWEDOFF:
		return ("Sawed-Off");
	case WEAPON_TEC9:
		return ("Tec-9");
	case WEAPON_TASER:
		return ("Taser");
	case WEAPON_HKP2000:
		return ("HKP2000");
	case WEAPON_MP7:
		return ("MP7");
	case WEAPON_MP9:
		return ("MP9");
	case WEAPON_NOVA:
		return ("Nova");
	case WEAPON_P250:
		return ("P250");
	case WEAPON_SCAR20:
		return ("SCAR-20");
	case WEAPON_SG556:
		return ("SG 553");
	case WEAPON_SSG08:
		return ("SSG 08");
	case WEAPONTYPE_KNIFE:
		return ("Knife");
	case WEAPON_FLASHBANG:
		return ("Flashbang");
	case WEAPON_HEGRENADE:
		return ("HE Grenade");
	case WEAPON_SMOKEGRENADE:
		return ("Smoke");
	case WEAPON_MOLOTOV:
		return ("Molotov");
	case WEAPON_DECOY:
		return ("Decoy");
	case WEAPON_INCGRENADE:
		return ("Incendiary");
	case WEAPON_C4:
		return ("C4");
	case WEAPON_M4A1_SILENCER:
		return ("M4A1-S");
	case WEAPON_USP_SILENCER:
		return ("HKP2000");
	case WEAPON_CZ75A:
		return ("CZ75-Auto");
	default:
		return ("Knife");
	}
	return "No Detect";
}

std::string ClientState_Map()
{
	//return ReadMemory<std::string>(phandle, ClientState() + signatures::dwClientState_Map);
	char mapname[40] = "";
	DWORD Adr = ClientState() + signatures::dwClientState_Map;
	ReadProcessMemory(phandle, (void*)Adr, &mapname, sizeof(mapname), 0);
	return mapname;
}

std::string ClientState_MapDirectory()
{
	//return ReadMemory<char*>(phandle, ClientState() + signatures::dwClientState_MapDirectory);
	char mapname[40] = "";
	DWORD Adr = ClientState() + signatures::dwClientState_MapDirectory;
	ReadProcessMemory(phandle, (void*)Adr, &mapname, sizeof(mapname), 0);
	return mapname;
}

std::string GetGameDirectory()
{
	//return ReadMemory<char*>(phandle, ClientState() + signatures::dwClientState_MapDirectory);
	char mapname[180] = "";
	DWORD Adr = Engine + signatures::dwGameDir;
	ReadProcessMemory(phandle, (void*)Adr, &mapname, sizeof(mapname), 0);
	return mapname;
}

Vector2D GetClientState_ViewAngles()
{
	return ReadMemory<Vector2D>(phandle, ClientState() + signatures::dwClientState_ViewAngles);
}

void SetClientState_ViewAngles(Vector2D pos)
{
	WriteMemory<Vector2D>(phandle, ClientState() + signatures::dwClientState_ViewAngles, pos);
}

uint32_t  Getclientstate_last_outgoing_command( )
{
	return ReadMemory<int>(phandle, ClientState() + signatures::clientstate_last_outgoing_command);
}

// Memory Read Entity
int GetdwLocalPlayerTB()
{
	return ReadMemory<DWORD>(phandle, GetdwLocalPlayer() + netvars::m_iCrosshairId);
}

Vector3D GetOringin(int entity)
{
	return ReadMemory<Vector3D>(phandle, entity + netvars::m_vecOrigin);
}

Vector3D GetViewOffset(int entity)
{
	return ReadMemory<Vector3D>(phandle, entity + netvars::m_vecViewOffset);
}

Vector3D Geteyeposition(int entity)
{
	Vector3D ret;
	ret.X = GetOringin(entity).X + GetViewOffset(entity).X;
	ret.Y =GetOringin(entity).Y + GetViewOffset(entity).Y;
	ret.Z =GetOringin(entity).Z + GetViewOffset(entity).Z;
	return ret;
}

rn::vector3  GetOringinValve(int entity)
{
	return ReadMemory<rn::vector3 >(phandle, entity + netvars::m_vecOrigin);
}

rn::vector3  GetViewOffsetValve(int entity)
{
	return ReadMemory<rn::vector3 >(phandle, entity + netvars::m_vecViewOffset);
}

rn::vector3 GeteyepositionValve(int entity)
{
	return GetOringinValve(entity) + GetViewOffsetValve(entity);
}

int GetTeam(int entity)
{
	return ReadMemory<DWORD>(phandle, entity + netvars::m_iTeamNum);
}

int GetHealth(int entity)
{
	return ReadMemory<DWORD>(phandle, entity + netvars::m_iHealth);
}

int GetGlowIndex(int entity)
{
	return ReadMemory<DWORD>(phandle, entity + netvars::m_iGlowIndex);
}


int GetfFlags(int entity)
{
	return ReadMemory<DWORD>(phandle, entity + netvars::m_fFlags);
}

int GetbSpottedByMask(int entity)
{
	return ReadMemory<DWORD>(phandle, entity + netvars::m_bSpottedByMask);
}

int IsDormant(int entity)
{
	return ReadMemory<DWORD>(phandle, entity + signatures::m_bDormant) == 1;
}

int iShotsFired(int entity)
{
	return ReadMemory<DWORD>(phandle, entity + netvars::m_iShotsFired) == 1;
}

int Getindex()
{
	return ReadMemory<DWORD>(phandle, GetdwLocalPlayer() + 0x64);
}

Vector2D GetPunchAngle()
{
	return ReadMemory<Vector2D>(phandle, GetdwLocalPlayer() + netvars::m_aimPunchAngle);
}

BM GetBonePos(int entity, int id,int point)
{
	int bonePtr = ReadMemory<DWORD>(phandle, entity + netvars::m_dwBoneMatrix);
	return ReadMemory<BM>(phandle, bonePtr + (id * point));
}

float DistanceBetweenCross(float x, float y)
{
	float ydist = y - desktopHeight / 2;
	float xdist = x - desktopWidth / 2;
	return sqrt(std::pow(ydist,2) + std::pow(xdist , 2));
}

int Convert_To_Binary(int n)
{
	int binaryNumber = 0;
	int remainder, i = 1, step = 1;

	while (n != 0)
	{
		remainder = n % 2;
		n /= 2;
		binaryNumber += remainder * i;
		i *= 10;
	}
	return binaryNumber;
}

int get_intlen(int i)
{
	int len;
	if (i > 0) {
		for (len = 0; i > 0; len++) {
			i = i / 10;
		}
	}
	return len;
}

int get_intmid(int i, int b)
{
	if (i > 0) {
		for (int a = 0; a < b; a++) {
			i = i / 10;
		}
	}
	i = i % 10;
	return i;
}

int _IsVisibility(int n, int iLocalIndex)
{
	//In autoit i have use a string.
	int iLength = get_intlen(Convert_To_Binary(n));
	int sString = get_intmid(Convert_To_Binary(n), iLocalIndex);//Buggie
	//int sString = get_intmid(Convert_To_Binary(n), iLength - iLocalIndex - 1);//Buggie
	return sString;
}

void SilentAntiAimLoop()
{
	while (true)
	{
		//SetSendPackets(1);
		Sleep(100);
		if (SilentAntiAimONOFF == 0)
		{
			continue;
		}
		/*SetSendPackets(0);
		Sleep(200);
		SetSendPackets(1);
		Sleep(100);*/
		Vector2D oldAngles = GetClientState_ViewAngles();
				SetSendPackets(0);
# define MULTIPLAYER_BACKUP 150
# define IN_ATTACK			(1 << 0)
# define IN_JUMP 			( 1 << 1 )
# define IN_DUCK 			( 1 << 2 )
# define IN_LEFT 			( 1 << 7 )
# define IN_RIGHT 			( 1 << 8 )
# define IN_MOVELEFT 		( 1 << 9 )
# define IN_MOVERIGHT 		( 1 << 10 )
# define IN_RUN 			( 1 << 12 )
		//SetSensitivityVal(0);
		if (GetSendPackets == 0)
		{
			continue;
		}
		SetSendPackets(0);
		int iUserCMDSequenceNumber = 0;
		auto iDesiredCmdNumber = Getclientstate_last_outgoing_command() + 2;
		auto pUserCmd = InputCMD() + ((iDesiredCmdNumber) % MULTIPLAYER_BACKUP) * sizeof(UserCmd_t);
		auto pOldUserCmd = InputCMD() + ((iDesiredCmdNumber - 1) % MULTIPLAYER_BACKUP) * sizeof(UserCmd_t);
		auto pVerifiedOldUserCmd = InputVer() + ((iDesiredCmdNumber - 1) % MULTIPLAYER_BACKUP) * sizeof(VerifiedUserCmd_t);
		while (ReadMemory<int32_t>(phandle, pUserCmd + 0x4) != iDesiredCmdNumber)// wait for the right usercmd to be accessed
		{
		}
		UserCmd_t OldUserCmd = ReadMemory<UserCmd_t>(phandle, pOldUserCmd);
		//std::cout << OldUserCmd.m_flUpmove<< std::endl;
		//OldUserCmd.m_flForwardmove = 0;
		//OldUserCmd.m_flSidemove = 0;
		//OldUserCmd.m_flUpmove = 0;
		//OldUserCmd.m_flSidemove = -450;
		//OldUserCmd.m_iButtons |= IN_ATTACK;
		//OldUserCmd.m_iButtons |= IN_DUCK;
		OldUserCmd.m_vecViewAngles.X = 89.f;
		OldUserCmd.m_vecViewAngles.Y = -180.f + rand() % 360;
		WriteMemory<UserCmd_t>(phandle, pOldUserCmd, OldUserCmd);
		WriteMemory<UserCmd_t>(phandle, pVerifiedOldUserCmd, OldUserCmd);
		Sleep(100);
		SetSendPackets(1);
		//Sleep(7);
	}


}

void FakeLagLoop()
{
	while (true)
	{
		Sleep(50);
		if (FakeLagONOFF == 0)
		{
			continue;
		}
		SetSendPackets(0);
		Sleep(250);
		SetSendPackets(1);
		Sleep(100);
	}
}

void GlowLoop()
{
	if (ClientState_MapDirectory() != lastmap && "" != ClientState_MapDirectory())
	{
		BSP.load_map(GetGameDirectory() + "//", ClientState_MapDirectory());
		lastmap = ClientState_MapDirectory();
	}
	while (true)
	{
		Sleep(10);
		if (WallHackONOFF == 0)
		{
			continue;
		}
		for (size_t i = 0; i <= 64; i++)
		{
			int currentEntity;
			currentEntity = ReadMemory<DWORD>(phandle, Client + signatures::dwEntityList + (i * 0x10));
			if (currentEntity == 0)
			{
				continue;
			}

			if (IsDormant(currentEntity))
			{
				continue;
			}

			if (GetHealth(currentEntity) < 1)
			{
				continue;
			}
			if (GetTeam(GetdwLocalPlayer()) == GetTeam(currentEntity))
			{
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x8, 0);
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0xC, 0.4);
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x10, 0);
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x14, 1);
				WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x28, 1);
				WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x29, 0);
				WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x2A, 0);

				continue;
			}
			WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x8, 0.4);
			
			
			if (!(GetbSpottedByMask(currentEntity) <= 0 && _IsVisibility(GetbSpottedByMask(currentEntity), Getindex() - 1) >= 0))
			{
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0xC, 0.4);
			}

			if (!BSP.is_visible(GeteyepositionValve(GetdwLocalPlayer()), GeteyepositionValve(currentEntity)) <= 0)
			{
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0xC, 0.2);
			}
			

			if (_IsVisibility(GetbSpottedByMask(currentEntity), Getindex() - 1) <= 0)
			{
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x10, 0);
			}
			else
			{
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0xC, 0.4);
				WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x10, 0.4);
			}


			WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x14, 1);
			WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x28, 1);
			WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x29, 0);
			WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x2A, 0);
			//WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x25, 0);
			//WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x26, 1);
		}
	}
}

void TriggerbotLoop()
{
	int currentEntity;
	while (true)
	{
		//std::cout << std::hex << GetdwLocalPlayer() << std::endl;
		Sleep(50);

		if (TiggerbotONOFF == 0)
		{
			continue;
		}
		

		if (GetdwLocalPlayerTB() > 0)
		{
			currentEntity = ReadMemory<DWORD>(phandle, Client + signatures::dwEntityList + (GetdwLocalPlayerTB() - 0x1) * 0x10);

			if (GetTeam(GetdwLocalPlayer()) == GetTeam(currentEntity))
			{
				continue;
			}
			if (internalexternal == 1 && iShotsFired(GetdwLocalPlayer()) == 0)
			{
				Sleep(rand() % TriggerDelayBaseDOWN + TriggerDelayBaseDOWN);
				ForceAttack(5);
				Sleep(rand() % TriggerDelayUP + TriggerDelayBaseUP);
				ForceAttack(4);
			}
			else
			{
			Sleep(rand() % TriggerDelayBaseDOWN + TriggerDelayBaseDOWN);
			mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
			Sleep(rand() % TriggerDelayUP + TriggerDelayBaseUP);
			mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
			}
		}
	}
}

void BhopLoop()
{
	while (true)
	{
	Sleep(50);
	
	if (BHopONOFF == 0 && !GetAsyncKeyState(BhopKey))
	{
			continue;
	}

	if (GetfFlags(GetdwLocalPlayer()) == 257)
	{	
		Sleep(rand() % BhopDelay + BhopDelayBase);
		keybd_event(VK_SPACE, 0x39, KEYEVENTF_EXTENDEDKEY, 0);
		Sleep(rand() % BhopDelay + BhopDelayBase);
		keybd_event(VK_SPACE, 0x39, KEYEVENTF_KEYUP, 0);
	}
		continue;
	}
}

/*
int HitChance()
{

	// Total rays to calculate
	int rayAmount = 200;

	// Counted hits
	int hits = 0;

	// Get view angles and eyepos (duh)
	auto viewAngles = GetClientState_ViewAngles();
	Vector3D start = GeteyepositionValve(GetdwLocalPlayer());

	// Calculate weapon spread angle
	auto spreadAngle = M_RAD2DEG(weapon->GetInaccuracy() + weapon->GetSpread());

	for (int i = 0; i < rayAmount; i++) {

		// Ratio of the loop (0 at beginning, 1 at end)
		float ratio = (i / (float)rayAmount);

		// This is the spread angle distance
		float multiplier = ratio * spreadAngle;

		// Calculate spread direction spiral (30 is a number I chose, you can test other numbers too)
		// Using sqrtf to make our spread circle exponentially distributed
		float spreadDir = sqrtf(ratio) * PI * 30;

		// Now we can calculate our actual spread angle
		Angle spreadAngle = Angle(cos(spreadDir) * multiplier, sin(spreadDir) * multiplier, 0);

		// The tested shot angle (spread added to view angles)
		Angle shotAngle = viewAngles + spreadAngle;

		// Calculate where this simulated spread shot will end
		auto endpos = start + Math::AngleToVector(shotAngle) * weapon->GetWeaponData()->flRange;

		// An example of doing a trace check on a player
		// If you want good ragebot performance, I wouldn't do this
		TRay ray(start, endpos);
		TTrace tr; g_EngineTrace->ClipRayToEntity(ray, MASK_SHOT, player, &tr);
		if (tr.hitEntity == targetEntity) {
			// Count the hit
			hits++;
		}
	}

	return int;
}*/

BM GetBonePos(long long entity, int id, int pos)
{
	int bMatrix = ReadMemory<int>(phandle, entity + netvars::m_dwBoneMatrix);
	return ReadMemory<BM>(phandle, bMatrix + (id*pos));
}

Vector2D WorldToScreen(BM from)
{
	VM vMatrix = ReadMemory<VM>(phandle, Client + signatures::dwViewMatrix );
	Vector2D vec2;
	vec2.X = 0.0;
	vec2.Y = 0.0;

	float m41 = vMatrix.M41 * from.X + vMatrix.M42 * from.Y + vMatrix.M43 * from.Z + vMatrix.M44;
	if( m41 >= 0.01)
	{
	float single = 1 / m41;
	float m11 = (vMatrix.M11 *  from.X + vMatrix.M12 *  from.Y + vMatrix.M13 *  from.Z + vMatrix.M14) *  single;
	float m21 = (vMatrix.M21 *  from.X + vMatrix.M22 *  from.Y + vMatrix.M23 *  from.Z + vMatrix.M24) *  single;

	vec2.X = (m11 + 1) * 0.5* desktopWidth;
	vec2.Y = (m21 - 1) * -0.5* desktopHeight;
	}

	return vec2;
}

void updateBSP(void)
{
	/*if (!inGame)
	{
		//if not in game, make the mapName empty
		if (strcmp(mapName, "") != 0)
		{
			mapName[0] = '\0';
		}
	}
	else
	{*/
		//if (strcmp(mapName, "") == 0) //if we are in game and mapName is empty we need to update it
		{
		//char mapName;
		//ReadMemory<char*>(phandle, ClientState() + ClientState_Map());

			std::string path = "";

			char filename[MAX_PATH];
			if (GetModuleFileNameEx("csgo.exe", NULL, LPWSTR("dust_2"), MAX_PATH) == 0) {
				return;
			}

			std::string tmp = filename;
			int pos = tmp.find("csgo");
			tmp = tmp.substr(0, pos);
			tmp = tmp + "csgo\\maps\\" + "dust_2" + ".bsp";
			 //weird issues if we open *.bsp while loading the game, just sleep a bit until the game loads
		   // BSP->LoadBSP(tmp);
		}
	//}
}
// Convert a wide Unicode string to an UTF8 string
std::string utf8_encode(const std::wstring &wstr)
{
	if (wstr.empty()) return std::string();
	int size_needed = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), NULL, 0, NULL, NULL);
	std::string strTo(size_needed, 0);
	WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(), &strTo[0], size_needed, NULL, NULL);
	return strTo;
}

// Convert an UTF8 string to a wide Unicode String
std::wstring utf8_decode(const std::string &str)
{
	if (str.empty()) return std::wstring();
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
	return wstrTo;
}

int GetClosestTarget()
{
	int currentEntity = 0;
	int returnEntity = 0;
	double maxDistance = 99999999.0;
	if (ClientState_MapDirectory() != lastmap && "" != ClientState_MapDirectory())
	{
		BSP.load_map(GetGameDirectory() + "//", ClientState_MapDirectory());
		lastmap = ClientState_MapDirectory();
	}


	for (size_t i = 0; i <= 64; i++)
	{
		currentEntity = ReadMemory<DWORD>(phandle, Client + signatures::dwEntityList + (i * 0x10));
		if (currentEntity == 0)
		{
			continue;
		}

		if (IsDormant(currentEntity))
		{
			continue;
		}

		if (GetTeam(GetdwLocalPlayer()) == GetTeam(currentEntity))
		{
			continue;
		}

		if (GetHealth(currentEntity) < 1)
		{
			continue;
		}
		//Vector2D Angel = CalcAngle(ge, entitypos);
		//if (AngleDifference(GetClientState_ViewAngles(), Angel, 5.0) == 1)
		//{
		//	continue;
		//}
		if (VisibilityCheckONOFF == 1)
		{
		int indexnum = (_IsVisibility(GetbSpottedByMask(currentEntity), Getindex() - 1));
		//int indexnum = GetbSpottedByMask(currentEntity);
		if (indexnum <= 0)//AimbotWallAimONOFF == 0)
		{
			continue;
		}
		}
		//if (!bspParser->is_visible(localPosition, entityPosition))
		//{
		//	continue;
		//}
		//const auto is_visible = g_pBSPParser->is_visible(Geteyeposition(GetdwLocalPlayer()), Geteyeposition(currentEntity));
		//std::cout << "test :" << g_pBSPParser->is_visible(Geteyeposition(GetdwLocalPlayer()), Geteyeposition(currentEntity)) << std::endl;

		https://www.unknowncheats.me/forum/counterstrike-global-offensive/111750-hitbox-esp.html
		

		BM hitbox = GetBonePos(currentEntity, 0x30, 8);
		Vector2D w2sHitbox = WorldToScreen(hitbox);
			/*
			Valve::Vector3  entitypos;
			Valve::Vector3  entityposmin;
			Valve::Vector3  entityposmax;
			entitypos. = hitbox.X;
			entitypos.Y = hitbox.Y;
			entitypos.Z = hitbox.Z;
			entitypos.Z = entitypos.Z + (float)64;
			entityposmin.X = hitbox.X + entityposmin.X - 2.7713f;
			entityposmin.Y = hitbox.Y + entityposmin.Y - 2.8783f;
			entityposmin.Z = hitbox.Z + entityposmin.Z - 3.103f;
			entityposmin.Z = entityposmin.Z + (float)64;
			entityposmax.X = hitbox.X + entityposmax.X + 6.955;
			entityposmax.Y = hitbox.Y + entityposmax.Y + 3.5203;
			entityposmax.Z = hitbox.Z + entityposmax.Z + 3.0067;
			entityposmax.Z = entityposmax.Z + (float)64;*/

		if (VisibilityCheckONOFF == 2)
		{
				//-Hitbox[8].vMax - Hitbox[8].vMin

				/*Valve::Vector3 PointtoTrace = GeteyepositionValve(currentEntity);
				PointtoTrace(0) = -Hitbox[8].vMax.X;
				PointtoTrace(1) =- Hitbox[8].vMax.Y;
				PointtoTrace(2) =- Hitbox[8].vMax.Z;*/
				auto is_visible = BSP.is_visible(GeteyepositionValve(GetdwLocalPlayer()), GeteyepositionValve(currentEntity));

				//const auto is_visible = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), GeteyepositionValve(currentEntity));
				//const auto is_visible = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), entitypos);
				//const auto is_visiblemin = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), entityposmin);
				//const auto is_visiblemax = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), entityposmax);
				if (is_visible <= 0)
				{
					//WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x8, 0);
					//WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0xC, 0.4);
					//WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x10, 0);
					//WriteMemory<float>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x14, 1);
					//WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x28, 1);
					//WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x29, 0);
					//WriteMemory<BYTE>(phandle, GlowObjectManager() + (GetGlowIndex(currentEntity) * 0x38) + 0x2A, 0);
					continue;
				}
		}
		//if (is_visiblemin <= 0 && AimbotWallAimONOFF == 0)
		//{
		//	continue;
		//}
		//if (is_visiblemax <= 0 && AimbotWallAimONOFF == 0)
		//{
		//	continue;
		//}

		float distance = DistanceBetweenCross(w2sHitbox.X, w2sHitbox.Y);

		//float AimFOV = 360;
		if (distance < maxDistance)// <= AimFOV * 8)
		{
			maxDistance = distance;
			returnEntity = currentEntity;
		}

		//returnEntity = currentEntity;
		//loadmap  = ClientState_MapDirectory();
		//const auto is_visible = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), GeteyepositionValve(currentEntity));
		//std::cout << is_visible << std::endl;
		//std::cout << Geteyeposition(GetdwLocalPlayer()) << std::endl;
		//std::cout << Geteyeposition(currentEntity) << std::endl;
	}
	return returnEntity;
}

Vector2D ScreenToDelta(Vector2D from , float speed)
{
	float ScreenCenterX = desktopWidth / 2;
	float ScreenCenterY = desktopHeight / 2;
	float TargetX = 0;
	float TargetY = 0;
	if (from.X != 0)
	{
		if (from.X > ScreenCenterX)
		{
			TargetX = -(ScreenCenterX - from.X);
			TargetX /= speed;
			if (TargetX + ScreenCenterX > ScreenCenterX *2)
			{
				TargetX = 0;
			}
		}

		if (from.X < ScreenCenterX)
		{
			TargetX = from.X - ScreenCenterX;
			TargetX /= speed;
			if (TargetX + ScreenCenterX < 0)
			{
				TargetX = 0;
			}
		}
	}

	if (from.Y != 0)
	{
		if (from.Y > ScreenCenterY)
		{
			TargetY = -(ScreenCenterY - from.Y);
			TargetY /= speed;
			if (TargetY + ScreenCenterY > ScreenCenterY * 2)
			{
				TargetY = 0;
			}
		}

		if (from.Y < ScreenCenterY)
		{
			TargetY = from.Y - ScreenCenterY;
			TargetY /= speed;
			if (TargetY + ScreenCenterY < 0)
			{
				TargetY = 0;
			}
		}
	}

	Vector2D to;
	if (AimbotRageONOFF == 1)
	{
		to.X = TargetX;
		to.Y = TargetY;
		return to;
	}
	
	TargetX /= GetSensitivityVal();//rand() % RandomAimSpeedX + SensitivityBase;
	TargetY /= GetSensitivityVal(); //rand() % RandomAimSpeedY + SensitivityBase;
	TargetX /= SensitivityBase;//rand() % RandomAimSpeedX + SensitivityBase;
	TargetY /= SensitivityBase;

	if (fabs(TargetX) < 1)
	{
		TargetX *= SensitivityBase * GetSensitivityVal();
		if (TargetX > 0)
		{
			//TargetX = 1;
		}
		if (TargetX < 0)
		{
			//TargetX = -1;
		}
	}
	if (fabs(TargetY) < 1)
	{
		TargetY *= SensitivityBase * GetSensitivityVal();
		if (TargetY > 0)
		{
			//TargetY = 1;
		}
		if (TargetY < 0)
		{
			//TargetY = -1;
		}
	}

	to.X = TargetX;
	to.Y = TargetY;
	return to;
}

float AngleNormalize(float angle)
{
	angle = fmodf(angle, 360.0f);
	if (angle > 180)
	{
		angle -= 360;
	}
	if (angle < -180)
	{
		angle += 360;
	}
	return angle;
}

Vector2D CalcAngle(Vector3D& src, Vector3D& dst)
{
	Vector2D vAngle;
	D3DXVECTOR3 delta((src.X - dst.X), (src.Y - dst.Y), (src.Z - dst.Z));
	double hyp = sqrt(delta.x*delta.x + delta.y*delta.y);

	vAngle.X = (float)(atan2f((delta.z + 64.06f), hyp) * 57.295779513082f);//180 / PI = 57.295779513082f vAngle.X = (float)(asin((delta.z + 64.06f) / hyp) * 57.295779513082f)
	vAngle.Y = (float)(atanf(delta.y / delta.x) * 57.295779513082f);
	//vAngle.Z = 0.0f;

	if (delta.x >= 0)//-180.0f)//0.0)
		vAngle.Y += 180.0f;
	return vAngle;
}

Vector2D normalizeAngles(Vector2D& angles)
{
	while (angles.X > 89.f)
		angles.X -= 180.f;

	while (angles.X < -89.f)
		angles.X += 180.f;

	while (angles.Y > 180.f)
		angles.Y -= 360.f;

	while (angles.Y < -180.f)
		angles.Y += 360.f;
	return angles;
}

Vector2D clampAngles(Vector2D& angles)
{
	if (angles.X > 89.0)
		angles.X = 89.0;

	if (angles.X < -89.0)
		angles.X = -89.0;

	if (angles.Y > 180.0)
		angles.Y = 180.0;

	if (angles.Y < -180.0)
		angles.Y = -180.0;
	return angles;
}

float AngleDistance(Vector2D ViewAngles, Vector2D TargetAngles)
{
	float pitch = abs(ViewAngles.X - TargetAngles.X);
	float yaw = abs(ViewAngles.Y - TargetAngles.Y);
	return sqrt(powf(pitch, 2.0) + powf(yaw, 2.0));
}

bool AngleDifference(Vector2D ViewAngles, Vector2D TargetAngles, float distance)
{
	float pitch = abs(ViewAngles.X - TargetAngles.X);
	float yaw = abs(ViewAngles.Y - TargetAngles.Y);

	float fov = sqrt(powf(pitch, 2.0) + powf(yaw, 2.0));
	if (fov < distance)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


void Aimbotsilent(Vector2D Angel)
{
	if (IsAbleToShoot() == 1)
	{
	//	return;
	}
	//https://www.unknowncheats.me/forum/counterstrike-global-offensive/190744-low-fov-aimbot-help.html
	//https://www.unknowncheats.me/forum/counterstrike-global-offensive/183033-external-silentaim-doesnt-miss.html
	//https://www.unknowncheats.me/forum/counterstrike-global-offensive/137767-perfect-silent-aim-external.html
	//https://www.unknowncheats.me/forum/counterstrike-global-offensive/315732-silently-set-viewangles-external.html
	if (AngleDifference(GetClientState_ViewAngles(), Angel, SilentAimFOV) == 1)
	{
		Vector2D oldAngles = GetClientState_ViewAngles();
		//SetSensitivityVal(0);
		SetSendPackets(0);
#define MULTIPLAYER_BACKUP    150
		int iUserCMDSequenceNumber = 0;
		auto iDesiredCmdNumber = Getclientstate_last_outgoing_command() + 2;
		auto pUserCmd = InputCMD() + ((iDesiredCmdNumber) % MULTIPLAYER_BACKUP) * sizeof(UserCmd_t);
		auto pOldUserCmd = InputCMD() + ((iDesiredCmdNumber - 1) % MULTIPLAYER_BACKUP) * sizeof(UserCmd_t);
		auto pVerifiedOldUserCmd = InputVer() + ((iDesiredCmdNumber - 1) % MULTIPLAYER_BACKUP) * sizeof(VerifiedUserCmd_t);
		while (ReadMemory<int32_t>(phandle, pUserCmd + 0x4) < iDesiredCmdNumber)// wait for the right usercmd to be accessed
		//while (ReadMemory<int32_t>(phandle, pUserCmd + 0x4) != iDesiredCmdNumber)// wait for the right usercmd to be accessed
		{
		}
#define IN_ATTACK		(1 << 0)
#define IN_DUCK 		(1 << 2)
		UserCmd_t OldUserCmd = ReadMemory<UserCmd_t>(phandle, pOldUserCmd);
		//std::cout << OldUserCmd << std::endl;

		OldUserCmd.m_vecViewAngles.X = Angel.X;
		OldUserCmd.m_vecViewAngles.Y = Angel.Y;
		
		/*if (IsAccuracyPenalty() <= (float)0.00642) muss verbessert werden mit der items_game.txt
		{
			OldUserCmd.m_iButtons |= IN_ATTACK;
		}*/
		OldUserCmd.m_iButtons |= IN_ATTACK;

		//OldUserCmd.m_flForwardmove = 0;
		//OldUserCmd.m_iButtons |= IN_DUCK;

		WriteMemory<UserCmd_t>(phandle, pOldUserCmd, OldUserCmd);
		WriteMemory<UserCmd_t>(phandle, pVerifiedOldUserCmd, OldUserCmd);

		SetSendPackets(1);
		//SetClientState_ViewAngles(oldAngles);
		//SetSensitivityVal(2);
		Sleep(6);
	}
}

int Posfinder(int closestEntity, Vector2D punchAngle)
{
int returni = 0;
float maxDistance = 99999999.0;
//for (int i = 0; i < 80; i++)
for (int i = 3; i <= MaxBones; i++)
{
Vector3D playerpos;
playerpos = Geteyeposition(GetdwLocalPlayer());
Vector3D entitypos;
BM hitbox = GetBonePos(closestEntity, 0x30, i);//GetHitbox
//entitypos = Geteyeposition(closestEntity);
entitypos.X = hitbox.X;
entitypos.Y = hitbox.Y;
entitypos.Z = hitbox.Z;
entitypos.Z = entitypos.Z + (float)64;
Vector2D Angel = clampAngles(normalizeAngles(CalcAngle(playerpos, entitypos)));
Angel.X -= punchAngle.X*(float)2;
Angel.Y -= punchAngle.Y*(float)2;
	float distance = AngleDistance(clampAngles(normalizeAngles(GetClientState_ViewAngles())), Angel);
	//std::cout << "A: " << Angel.X << " B :" << clampAngles(normalizeAngles(GetClientState_ViewAngles())).X << std::endl;
	//std::cout << "A: " << Angel.Y << " B :" << clampAngles(normalizeAngles(GetClientState_ViewAngles())).Y << std::endl;
	//std::cout << "distance: " << distance << " durchlauf :" << i << std::endl;
	if (distance < maxDistance)// <= AimFOV * 8)
	{
		//std::cout << distance << std::endl;
		maxDistance = distance;
		returni = i;
	}
}
//std::cout << "Win : distance: " << maxDistance << " durchlauf :" << returni << std::endl;
return returni;
//BM hitbox = GetBonePos(closestEntity, 0x30, returni);//GetHitbox

}

void AimbotLoop()
{
	while (true)
	{
		Sleep(1);
		if (AimbotONOFF == 0 && !GetAsyncKeyState(AimbotKey))//Wait for key is pressed
		{
			continue;
		}

		int closestEntity = GetClosestTarget();//GetTargetEntity
		if (closestEntity == 0)
		{
			continue;
		}
		//RandomPos
		Vector2D punchAngle = GetPunchAngle();//GetPunchAngle
		BM hitbox;
		if (PosfinderONOFF == 1)
		{
			hitbox = GetBonePos(closestEntity, 0x30, Posfinder(closestEntity, punchAngle));
		}
		else
		{
			hitbox = GetBonePos(closestEntity, 0x30, 8);
		}
		
		/*for (size_t i = 0; i <= sizeof(Hitbox) / sizeof(Hitbox[0]); i++)
		{
			const auto is_visible = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), GeteyepositionValve(closestEntity) - Hitbox[8].vMax - Hitbox[8].vMin);
			//const auto is_visible = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), entitypos);
			//const auto is_visiblemin = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), entityposmin);
			//const auto is_visiblemax = g_pBSPParser->is_visible(GeteyepositionValve(GetdwLocalPlayer()), entityposmax);
			if (is_visible <= 0 && i == 21)
			{
				continue;
			}
		}*/

		Vector2D w2sHitbox = WorldToScreen(hitbox);//Convert Hitbox to Screen
		if (w2sHitbox.X == 0 && w2sHitbox.Y == 0)//Check Hitbox not is Null
		{
			if (internalexternal == 0)
			{
				continue;
			}
		}

		//Get Screen resulut
		int dx = desktopWidth / 90;
		int dy = desktopHeight / 90;

		//Rand Recoil#
		if (AimbotRageONOFF == 0 && punchAngle.X > AimRecoilRandomX + AimRecoilBase && punchAngle.Y > AimRecoilRandomY + AimRecoilBase)
		{
			//punchAngle.Y =- rand() % AimRecoilRandomX + AimRecoilBase;
			//punchAngle.X =- rand() % AimRecoilRandomY + AimRecoilBase;
		}
		w2sHitbox.X += dx * punchAngle.Y;//NoRecoil
		w2sHitbox.Y -= dy * punchAngle.X;

		Vector2D aimPoint = ScreenToDelta(w2sHitbox, 1);//Convert to Screen Mouse
		//Mousemove on aimPoint

		Vector3D playerpos;
		playerpos = Geteyeposition(GetdwLocalPlayer());
		Vector3D entitypos;
		if (internalexternal == 1)
		{
			//entitypos = Geteyeposition(closestEntity);
			entitypos.X = hitbox.X;
			entitypos.Y = hitbox.Y;
			entitypos.Z = hitbox.Z;
			entitypos.Z = entitypos.Z + (float)64;
			Vector2D Angel = CalcAngle(playerpos, entitypos);
			//NoRecoil
			Angel.X -= punchAngle.X*(float)2;
			Angel.Y -= punchAngle.Y*(float)2;
			//normalizeAngles clampAngles
			Angel = normalizeAngles(Angel);
			Angel = clampAngles(Angel);
			//No Null
			if (Angel.X != Angel.X || Angel.Y != Angel.Y)
			{
				continue;
			}
			Vector2D viewAngles = GetClientState_ViewAngles();


			float dYawTowardsRight = viewAngles.Y - Angel.Y;
			while (dYawTowardsRight < 0)
				dYawTowardsRight += 360.f;
			while (dYawTowardsRight > 360.f)
				dYawTowardsRight -= 360.f;
			float dYawTowardsLeft = dYawTowardsRight - 360.f;

			Vector2D dAngles;
			dAngles.X = Angel.X - viewAngles.X;
			dAngles.Y = Angel.Y - viewAngles.Y;
			if (abs(dYawTowardsRight) < abs(dYawTowardsLeft))
				dAngles.Y = -dYawTowardsRight;
			else
				dAngles.Y = -dYawTowardsLeft;
			//Random Speed
			if (AimbotRageONOFF == 0)
			{
				dAngles.Y /= rand() % RandomAimSpeedY + BaseRandomAimSpeedY;
				dAngles.X /= rand() % RandomAimSpeedX + BaseRandomAimSpeedX;
			}
			else
			{
				dAngles.Y /= RandomAimSpeedY;
				dAngles.X /= RandomAimSpeedX;
			}

			Vector2D smoothTargetAngles;
			smoothTargetAngles.X = viewAngles.X + dAngles.X;
			smoothTargetAngles.Y = viewAngles.Y + dAngles.Y;

			smoothTargetAngles = normalizeAngles(smoothTargetAngles);
			smoothTargetAngles = clampAngles(smoothTargetAngles);

			Angel = normalizeAngles(Angel);
			Angel = clampAngles(Angel);
			if (AngleDifference(GetClientState_ViewAngles(), Angel, AimFOV) == 1)
			{
				SetClientState_ViewAngles(smoothTargetAngles);
			}
			Aimbotsilent(Angel);
			//SetClientState_ViewAngles(Angel);
			/*if (iShotsFired(GetdwLocalPlayer()) == 0 && AimbotRageONOFF == 1 && aimPoint.X < 2 && aimPoint.Y < 2)
			{
				ForceAttack(5);
				Sleep(20);
				ForceAttack(4);
			}
			else
			{
			//	ForceAttack(4);
			}*/
		}
		else
		{


				//if (AimbotRageONOFF == true && aimPoint.X < 1 && aimPoint.Y < 1 && iShotsFired(GetdwLocalPlayer()) == 0)
				//{
				//	Vector2D oldvec;
				//	//https://www.unknowncheats.me/forum/counterstrike-global-offensive/183033-external-silentaim-doesnt-miss.html
				//	//https://www.unknowncheats.me/forum/counterstrike-global-offensive/137767-perfect-silent-aim-external.html
				//aimPoint.X /= GetSensitivityVal();
				//aimPoint.Y /= GetSensitivityVal();
			mouse_event(MOUSEEVENTF_MOVE, aimPoint.X, aimPoint.Y, 0, 0);
				//}

			if (AimbotRageONOFF == true && aimPoint.X < 1 && aimPoint.Y < 1 && iShotsFired(GetdwLocalPlayer()) == 0)
			{
				mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);
				Sleep(1);
				mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);
			}
		}
	}
}

void loadconfig()
{
	internalexternal = configread("Intenal", "config.txt");
	SensitivityBase = configread("SensitivityBase", "config.txt");
	BaseRandomAimSpeedX = configread("BaseRandomAimSpeedX", "config.txt");
	BaseRandomAimSpeedY = configread("BaseRandomAimSpeedY", "config.txt");
	RandomAimSpeedX = configread("RandomAimSpeedX", "config.txt");
	RandomAimSpeedY = configread("RandomAimSpeedY", "config.txt");
	AimRecoilBase = configread("AimRecoilBase", "config.txt");
	AimRecoilRandomX = configread("AimRecoilRandomX", "config.txt");
	AimRecoilRandomY = configread("TriggerDelayBaseDOWN", "config.txt");
	SilentAimFOV = configread("SilentAimFOV", "config.txt");
	AimFOV = configread("AimFOV", "config.txt");
	PosfinderONOFF = configread("PosfinderONOFF", "config.txt");
	TriggerDelayDOWN = configread("TriggerDelayDOWN", "config.txt");
	TriggerDelayBaseDOWN = configread("TriggerDelayBaseUP", "config.txt");
	TriggerDelayBaseUP = configread("TriggerDelayBaseUP", "config.txt");
	TriggerDelayUP = configread("TriggerDelayUP", "config.txt");
	BhopDelayBase = configread("BhopDelayBase", "config.txt");
	BhopDelay = configread("BhopDelay", "config.txt");
	BhopKey = configread("BhopKey", "config.txt");
	AimbotKey = configread("AimbotKey", "config.txt");
	VisibilityCheckONOFF = configread("VisibilityCheck", "config.txt");
	MaxBones = configread("MaxBones", "config.txt");
}

void loadrageconfig()
{
	internalexternal = configread("Intenal", "rconfig.txt");
	SensitivityBase = configread("SensitivityBase", "rconfig.txt");
	BaseRandomAimSpeedX = configread("BaseRandomAimSpeedX", "rconfig.txt");
	BaseRandomAimSpeedY = configread("BaseRandomAimSpeedY", "rconfig.txt");
	RandomAimSpeedX = configread("RandomAimSpeedX", "rconfig.txt");
	RandomAimSpeedY = configread("RandomAimSpeedY", "rconfig.txt");
	AimRecoilBase = configread("AimRecoilBase", "rconfig.txt");
	AimRecoilRandomX = configread("AimRecoilRandomX", "rconfig.txt");
	AimRecoilRandomY = configread("TriggerDelayBaseDOWN", "rconfig.txt");
	SilentAimFOV = configread("SilentAimFOV", "rconfig.txt");
	AimFOV = configread("AimFOV", "rconfig.txt");
	PosfinderONOFF = configread("PosfinderONOFF", "rconfig.txt");
	TriggerDelayDOWN = configread("TriggerDelayDOWN", "rconfig.txt");
	TriggerDelayBaseDOWN = configread("TriggerDelayBaseUP", "rconfig.txt");
	TriggerDelayBaseUP = configread("TriggerDelayBaseUP", "rconfig.txt");
	TriggerDelayUP = configread("TriggerDelayUP", "rconfig.txt");
	BhopDelayBase = configread("BhopDelayBase", "rconfig.txt");
	BhopDelay = configread("BhopDelay", "rconfig.txt");
	BhopKey = configread("BhopKey", "rconfig.txt");
	AimbotKey = configread("AimbotKey", "config.txt");
	VisibilityCheckONOFF = configread("VisibilityCheck", "rconfig.txt");
	MaxBones = configread("MaxBones", "rconfig.txt");
}

void ONOFF(bool *a, char* b)
{
	if (*a == 0)
	{
		*a = 1;
		Sleep(500);
		std::cout << b << " On" << std::endl;
	}
	else
	{
		*a = 0;
		Sleep(500);
		std::cout << b << " Off" << std::endl;
	}
}


void VisibilityCheck(int a)
{
	VisibilityCheckONOFF = a;
}

void Triggerbot()
{
	ONOFF(&TiggerbotONOFF, "Trigger Bot");
}

void Aimbot()
{
	ONOFF(&AimbotONOFF, "Aimbotbot");
}

void AimbotRage()
{
	ONOFF(&AimbotRageONOFF, "Aimbotbot Rage");
	if (AimbotRageONOFF == 1)
	{
		loadrageconfig();
	}
	else
	{
		loadconfig();
	}
}

void WallHack()
{
	ONOFF(&WallHackONOFF, "WallHack");
}

void BHop()
{
	ONOFF(&BHopONOFF, "BHop");
}

void SilentAntiAim()
{
	ONOFF(&SilentAntiAimONOFF, "SilentAntiAim");
}

void FakeLag()
{
	ONOFF(&FakeLagONOFF, "FakeLag");
}

DWORD pid;
HWND hwnd;

void MemoryLoad()
{
	wchar_t* ClientDll = L"client.dll";
	wchar_t* EngineDll = L"engine.dll";
	wchar_t* FensterName = L"Counter-Strike: Global Offensive - Direct3D 9";
	wchar_t* ProzessName = L"csgo.exe";

	hwnd = FindWindow(NULL, FensterName);
	Client = GetModulBaseAddr(ProzessName, ClientDll);
	Engine = GetModulBaseAddr(ProzessName, EngineDll);
	std::cout << Base << "\n";	

	if (!hwnd)
	{
		std::cout << "Window not found!\n";
		std::cout << "Pls run CS GO, and go in the Singelplayer.\n";
		std::cout << "Run the exe as admin.\n";
		std::cin.get();
	}
	else
	{
		GetWindowThreadProcessId(hwnd, &pid);
		if (internalexternal == 1)
		{
			//internalexternal = 0;
			phandle = OpenProcess(PROCESS_VM_READ | PROCESS_VM_WRITE | PROCESS_VM_OPERATION, 0, pid);// Detect
		}
		else
		{
			phandle = OpenProcess(PROCESS_VM_READ, 0, pid);
		}
		//HANDLE BASE = OpenProcess(PROCESS_VM_READ, 0, pid);
		//CloseHandle(phandle);

		SetForegroundWindow(hwnd);
		std::cout << "External GO Version 1.3" << "\n";
		
		std::cout << "Nampad - hide and show" << "\n";
		std::cout << "Nampad 8 go up" << "\n";
		std::cout << "Nampad 5 select" << "\n";
		std::cout << "Nampad 6 right " << "\n";
		std::cout << "Nampad 4 left" << "\n";
		std::cout << "Nampad 2 go down" << "\n";
		std::cout << "Nampad 0 go bake" << "\n";
		
		/*dwClientState = PointerScanPatternEx(phandle, Engine, SIZE_, "\xA1\x00\x00\x00\x00\x33\xD2\x6A\x00\x6A\x00\x33\xC9\x89\xB0", "x????xxx?x?xxxx", 1, 0);
		dwClientState_GetLocalPlayer = PointerScanPatternEx(phandle, Engine, SIZE_, "\x8B\x80\x00\x00\x00\x00\x40\xC3", "xx????xx", 2, 0);
		dwClientState_IsHLTV = PointerScanPatternEx(phandle, Engine, SIZE_, "\x80\xBF\x00\x00\x00\x00\x00\x0F\x84\x00\x00\x00\x00\x32\xDB", "xx?????xx????xx", 2, 0);
		dwClientState_Map = PointerScanPatternEx(phandle, Engine, SIZE_, "\x05\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xA1", "x????xxxxxxxxx", 1, 0);
		dwClientState_MapDirectory = PointerScanPatternEx(phandle, Engine, SIZE_, "\xB8\x00\x00\x00\x00\xC3\x05\x00\x00\x00\x00\xC3", "x????xx????x", 7, 0);
		dwClientState_MaxPlayer = PointerScanPatternEx(phandle, Engine, SIZE_, "\xA1\x00\x00\x00\x00\x8B\x80\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\x55\x8B\xEC\x8A\x45\x08", "x????xx????xxxxxxxxxxx", 7, 0);
		dwClientState_PlayerInfo = PointerScanPatternEx(phandle, Engine, SIZE_, "\x8B\x89\x00\x00\x00\x00\x85\xC9\x0F\x84\x00\x00\x00\x00\x8B\x01", "xx????xxxx????xx", 2, 0);
		dwClientState_State = PointerScanPatternEx(phandle, Engine, SIZE_, "\x83\xB8\x00\x00\x00\x00\x00\x0F\x94\xC0\xC3", "xx?????xxxx", 2, 0);
		dwClientState_ViewAngles = PointerScanPatternEx(phandle, Engine, SIZE_, "\xF3\x0F\x11\x86\x00\x00\x00\x00\xF3\x0F\x10\x44\x24\x00\xF3\x0F\x11\x86", "xxxx????xxxxx?xxxx", 4, 0);
		clientstate_delta_ticks = PointerScanPatternEx(phandle, Engine, SIZE_, "\xC7\x87\x00\x00\x00\x00\x00\x00\x00\x00\xFF\x15\x00\x00\x00\x00\x83\xC4\x08", "xx????????xx????xxx", 2, 0);
		clientstate_last_outgoing_command = PointerScanPatternEx(phandle, Engine, SIZE_, "\x8B\x8F\x00\x00\x00\x00\x8B\x87\x00\x00\x00\x00\x41", "xx????xx????x", 2, 0);
		clientstate_choked_commands = PointerScanPatternEx(phandle, Engine, SIZE_, "\x8B\x87\x00\x00\x00\x00\x41", "xx????x", 2, 0);
		clientstate_net_channel = PointerScanPatternEx(phandle, Engine, SIZE_, "\x8B\x8F\x00\x00\x00\x00\x8B\x01\x8B\x40\x18", "xx????xxxxx", 2, 0);
		dwEntityList = PointerScanPatternEx(phandle, Client, SIZE_, "\xBB\x00\x00\x00\x00\x83\xFF\x01\x0F\x8C\x00\x00\x00\x00\x3B\xF8", "x????xxxxx????xx", 1, 0);
		dwForceAttack = PointerScanPatternEx(phandle, Client, SIZE_, "\x89\x0D\x00\x00\x00\x00\x8B\x0D\x00\x00\x00\x00\x8B\xF2\x8B\xC1\x83\xCE\x04", "xx????xx????xxxxxxx", 2, 0);
		dwForceAttack2 = PointerScanPatternEx(phandle, Client, SIZE_, "\x89\x0D\x00\x00\x00\x00\x8B\x0D\x00\x00\x00\x00\x8B\xF2\x8B\xC1\x83\xCE\x04", "xx????xx????xxxxxxx", 2, 12);
		dwForceBackward = PointerScanPatternEx(phandle, Client, SIZE_, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 287, 0);
		dwForceForward = PointerScanPatternEx(phandle, Client, SIZE_, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 245, 0);
		dwForceJump = PointerScanPatternEx(phandle, Client, SIZE_, "\x8B\x0D\x00\x00\x00\x00\x8B\xD6\x8B\xC1\x83\xCA\x02", "xx????xxxxxxx", 2, 0);
		dwForceLeft = PointerScanPatternEx(phandle, Client, SIZE_, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 465, 0);
		dwForceRight = PointerScanPatternEx(phandle, Client, SIZE_, "\x55\x8B\xEC\x51\x53\x8A\x5D\x08", "xxxxxxxx", 512, 0);
		dwGameDir = PointerScanPatternEx(phandle, Engine, SIZE_, "\x68\x00\x00\x00\x00\x8D\x85\x00\x00\x00\x00\x50\x68\x00\x00\x00\x00\x68", "x????xx????xx????x", 1, 0);
		dwGameRulesProxy = PointerScanPatternEx(phandle, Client, SIZE_, "\xA1\x00\x00\x00\x00\x85\xC0\x0F\x84\x00\x00\x00\x00\x80\xB8\x00\x00\x00\x00\x00\x74\x7A", "x????xxxx????xx?????xx", 1, 0);
		dwGetAllClasses = PointerScanPatternEx(phandle, Client, SIZE_, "\xA1\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xA1\x00\x00\x00\x00\xB9", "x????xxxxxxxxxxxx????x", 1, 0);
		dwGlobalVars = PointerScanPatternEx(phandle, Engine, SIZE_, "\x68\x00\x00\x00\x00\x68\x00\x00\x00\x00\xFF\x50\x08\x85\xC0", "x????x????xxxxx", 1, 0);
		dwGlowObjectManager = PointerScanPatternEx(phandle, Client, SIZE_, "\xA1\x00\x00\x00\x00\xA8\x01\x75\x4B", "x????xxxx", 1, 4);
		dwInput = PointerScanPatternEx(phandle, Client, SIZE_, "\xB9\x00\x00\x00\x00\xF3\x0F\x11\x04\x24\xFF\x50\x10", "x????xxxxxxxx", 1, 0);
		dwInterfaceLinkList = PointerScanPatternEx(phandle, Client, SIZE_, "\x8B\x35\x00\x00\x00\x00\x57\x85\xF6\x74\x00\x8B\x7D\x08\x8B\x4E\x04\x8B\xC7\x8A\x11\x3A\x10", "xx????xxxx?xxxxxxxxxxxx", 0, 0);
		dwLocalPlayer = PointerScanPatternEx(phandle, Client, SIZE_, "\x8D\x34\x85\x00\x00\x00\x00\x89\x15\x00\x00\x00\x00\x8B\x41\x08\x8B\x48\x04\x83\xF9\xFF", "xxx????xx????xxxxxxxxx", 3, 4);
		dwMouseEnable = PointerScanPatternEx(phandle, Client, SIZE_, "\xB9\x00\x00\x00\x00\xFF\x50\x34\x85\xC0\x75\x10", "x????xxxxxxx", 1, 48);
		dwMouseEnablePtr = PointerScanPatternEx(phandle, Client, SIZE_, "\xB9\x00\x00\x00\x00\xFF\x50\x34\x85\xC0\x75\x10", "x????xxxxxxx", 1, 0);
		dwPlayerResource = PointerScanPatternEx(phandle, Client, SIZE_, "\x8B\x3D\x00\x00\x00\x00\x85\xFF\x0F\x84\x00\x00\x00\x00\x81\xC7", "xx????xxxx????xx", 2, 0);
		dwRadarBase = PointerScanPatternEx(phandle, Client, SIZE_, "\xA1\x00\x00\x00\x00\x8B\x0C\xB0\x8B\x01\xFF\x50\x00\x46\x3B\x35\x00\x00\x00\x00\x7C\xEA\x8B\x0D", "x????xxxxxxx?xxx????xxxx", 1, 0);
		dwSensitivity = PointerScanPatternEx(phandle, Client, SIZE_, "\x81\xF9\x00\x00\x00\x00\x75\x1D\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x44\x24\x00\x8B\x44\x24\x0C\x35\x00\x00\x00\x00\x89\x44\x24\x0C", "xx????xxxxxx????xxxxx?xxxxx????xxxx", 2, 44);
		dwSensitivityPtr = PointerScanPatternEx(phandle, Client, SIZE_, "\x81\xF9\x00\x00\x00\x00\x75\x1D\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x44\x24\x00\x8B\x44\x24\x0C\x35\x00\x00\x00\x00\x89\x44\x24\x0C", "xx????xxxxxx????xxxxx?xxxxx????xxxx", 2, 0);
		dwSetClanTag = PointerScanPatternEx(phandle, Engine, SIZE_, "\x53\x56\x57\x8B\xDA\x8B\xF9\xFF\x15", "xxxxxxxxx", 0, 0);
		dwViewMatrix = PointerScanPatternEx(phandle, Client, SIZE_, "\x0F\x10\x05\x00\x00\x00\x00\x8D\x85\x00\x00\x00\x00\xB9", "xxx????xx????x", 3, 176);
		dwWeaponTable = PointerScanPatternEx(phandle, Client, SIZE_, "\xB9\x00\x00\x00\x00\x6A\x00\xFF\x50\x08\xC3", "x????x?xxxx", 1, 0);
		dwWeaponTableIndex = PointerScanPatternEx(phandle, Client, SIZE_, "\x39\x86\x00\x00\x00\x00\x74\x06\x89\x86\x00\x00\x00\x00\x8B\x86", "xx????xxxx????xx", 2, 0);
		dwYawPtr = PointerScanPatternEx(phandle, Client, SIZE_, "\x81\xF9\x00\x00\x00\x00\x75\x16\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x45\x00\x81\x75\x00\x00\x00\x00\x00\xEB\x0A\x8B\x01\x8B\x40\x30\xFF\xD0\xD9\x5D\x0C\x8B\x55\x08", "xx????xxxxxx????xxxx?xx?????xxxxxxxxxxxxxxx", 2, 0);
		dwZoomSensitivityRatioPtr = PointerScanPatternEx(phandle, Client, SIZE_, "\x81\xF9\x00\x00\x00\x00\x75\x1A\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x45\x00\x8B\x45\xF4\x35\x00\x00\x00\x00\x89\x45\xFC\xEB\x0A\x8B\x01\x8B\x40\x30\xFF\xD0\xD9\x5D\xFC\xA1", "xx????xxxxxx????xxxx?xxxx????xxxxxxxxxxxxxxxx", 2, 0);
		dwbSendPackets = PointerScanPatternEx(phandle, Engine, SIZE_, "\xB3\x01\x8B\x01\x8B\x40\x10\xFF\xD0\x84\xC0\x74\x0F\x80\xBF\x00\x00\x00\x00\x00\x0F\x84", "xxxxxxxxxxxxxxx?????xx", 0, 1);
		dwppDirect3DDevice9 = PointerScanPatternEx(phandle, Client, SIZE_, "\xA1\x00\x00\x00\x00\x50\x8B\x08\xFF\x51\x0C", "x????xxxxxx", 1, 0);
		m_pStudioHdr = PointerScanPatternEx(phandle, Client, SIZE_, "\x8B\xB6\x00\x00\x00\x00\x85\xF6\x74\x05\x83\x3E\x00\x75\x02\x33\xF6\xF3\x0F\x10\x44\x24", "xx????xxxxxx?xxxxxxxxx", 2, 0);
		m_yawClassPtr = PointerScanPatternEx(phandle, Client, SIZE_, "\x81\xF9\x00\x00\x00\x00\x75\x16\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x45\x00\x81\x75\x00\x00\x00\x00\x00\xEB\x0A\x8B\x01\x8B\x40\x30\xFF\xD0\xD9\x5D\x0C\x8B\x55\x08", "xx????xxxxxx????xxxx?xx?????xxxxxxxxxxxxxxx", 2, 0);
		m_pitchClassPtr = PointerScanPatternEx(phandle, Client, SIZE_, "\xA1\x00\x00\x00\x00\x89\x74\x24\x28", "x????xxxx", 1, 0);
		interface_engine_cvar = PointerScanPatternEx(phandle, Client, SIZE_, "\x8B\x0D\x00\x00\x00\x00\xC7\x05", "xx????xx", 2, 0);
		convar_name_hash_table = PointerScanPatternEx(phandle, Client, SIZE_, "\x8B\x3C\x85", "xxx", 3, 0);
		m_bDormant = PointerScanPatternEx(phandle, Client, SIZE_, "\x8A\x81\x00\x00\x00\x00\xC3\x32\xC0", "xx????xxx", 2, 8);
		model_ambient_min = PointerScanPatternEx(phandle, Engine, SIZE_, "\xF3\x0F\x10\x0D\x00\x00\x00\x00\xF3\x0F\x11\x4C\x24\x00\x8B\x44\x24\x20\x35\x00\x00\x00\x00\x89\x44\x24\x0C", "xxxx????xxxxx?xxxxx????xxxx", 4, 0);
		set_abs_angles = PointerScanPatternEx(phandle, Client, SIZE_, "\x55\x8B\xEC\x83\xE4\xF8\x83\xEC\x64\x53\x56\x57\x8B\xF1\xE8", "xxxxxxxxxxxxxxx", 0, 0);
		set_abs_origin = PointerScanPatternEx(phandle, Client, SIZE_, "\x55\x8B\xEC\x83\xE4\xF8\x51\x53\x56\x57\x8B\xF1\xE8", "xxxxxxxxxxxxx", 0, 0);
		is_c4_owner = PointerScanPatternEx(phandle, Client, SIZE_, "\x56\x8B\xF1\x85\xF6\x74\x31", "xxxxxxx", 0, 0);
		force_update_spectator_glow = PointerScanPatternEx(phandle, Client, SIZE_, "\x74\x07\x8B\xCB\xE8\x00\x00\x00\x00\x83\xC7\x10", "xxxxx????xxx", 0, 0);
		anim_overlays = PointerScanPatternEx(phandle, Client, SIZE_, "\x8B\x89\x00\x00\x00\x00\x8D\x0C\xD1", "xx????xxx", 2, 0);
		m_flSpawnTime = PointerScanPatternEx(phandle, Client, SIZE_, "\x89\x86\x00\x00\x00\x00\xE8\x00\x00\x00\x00\x80\xBE\x00\x00\x00\x00\x00", "xx????x????xx?????", 2, 0);
		find_hud_element = PointerScanPatternEx(phandle, Client, SIZE_, "\x55\x8B\xEC\x53\x8B\x5D\x08\x56\x57\x8B\xF9\x33\xF6\x39\x77\x28", "xxxxxxxxxxxxxxxx", 0, 0);
	*/
		std::ifstream file("./csgo.json");
		if (file.is_open())
		{
			json update = json::parse(file);
			signatures::dwClientState = update["signatures"]["dwClientState"];
			signatures::dwClientState_GetLocalPlayer = update["signatures"]["dwClientState_GetLocalPlayer"];
			signatures::dwClientState_IsHLTV = update["signatures"]["dwClientState_IsHLTV"];
			signatures::dwClientState_Map = update["signatures"]["dwClientState_Map"];
			signatures::dwClientState_MapDirectory = update["signatures"]["dwClientState_MapDirectory"];
			signatures::dwClientState_MaxPlayer = update["signatures"]["dwClientState_MaxPlayer"];
			signatures::dwClientState_PlayerInfo = update["signatures"]["dwClientState_PlayerInfo"];
			signatures::dwClientState_State = update["signatures"]["dwClientState_State"];
			signatures::dwClientState_ViewAngles = update["signatures"]["dwClientState_ViewAngles"];
			signatures::clientstate_delta_ticks = update["signatures"]["clientstate_delta_ticks"];
			signatures::clientstate_last_outgoing_command = update["signatures"]["clientstate_last_outgoing_command"];
			signatures::clientstate_choked_commands = update["signatures"]["clientstate_choked_commands"];
			signatures::clientstate_net_channel = update["signatures"]["clientstate_net_channel"];
			signatures::dwEntityList = update["signatures"]["dwEntityList"];
			signatures::dwForceAttack = update["signatures"]["dwForceAttack"];
			signatures::dwForceAttack2 = update["signatures"]["dwForceAttack2"];
			signatures::dwForceBackward = update["signatures"]["dwForceBackward"];
			signatures::dwForceForward = update["signatures"]["dwForceForward"];
			signatures::dwForceJump = update["signatures"]["dwForceJump"];
			signatures::dwForceLeft = update["signatures"]["dwForceLeft"];
			signatures::dwForceRight = update["signatures"]["dwForceRight"];
			signatures::dwGameDir = update["signatures"]["dwGameDir"];
			signatures::dwGameRulesProxy = update["signatures"]["dwGameRulesProxy"];
			signatures::dwGetAllClasses = update["signatures"]["dwGetAllClasses"];
			signatures::dwGlobalVars = update["signatures"]["dwGlobalVars"];
			signatures::dwGlowObjectManager = update["signatures"]["dwGlowObjectManager"];
			signatures::dwInput = update["signatures"]["dwInput"];
			signatures::dwInterfaceLinkList = update["signatures"]["dwInterfaceLinkList"];
			signatures::dwLocalPlayer = update["signatures"]["dwLocalPlayer"];
			signatures::dwMouseEnable = update["signatures"]["dwMouseEnable"];
			signatures::dwMouseEnablePtr = update["signatures"]["dwMouseEnablePtr"];
			signatures::dwPlayerResource = update["signatures"]["dwPlayerResource"];
			signatures::dwRadarBase = update["signatures"]["dwRadarBase"];
			signatures::dwSensitivity = update["signatures"]["dwSensitivity"];
			signatures::dwSensitivityPtr = update["signatures"]["dwSensitivityPtr"];
			signatures::dwSetClanTag = update["signatures"]["dwSetClanTag"];
			signatures::dwViewMatrix = update["signatures"]["dwViewMatrix"];
			signatures::dwWeaponTable = update["signatures"]["dwWeaponTable"];
			signatures::dwWeaponTableIndex = update["signatures"]["dwWeaponTableIndex"];
			signatures::dwYawPtr = update["signatures"]["dwYawPtr"];
			signatures::dwZoomSensitivityRatioPtr = update["signatures"]["dwZoomSensitivityRatioPtr"];
			signatures::dwbSendPackets = update["signatures"]["dwbSendPackets"];
			signatures::dwppDirect3DDevice9 = update["signatures"]["dwppDirect3DDevice9"];
			signatures::m_pStudioHdr = update["signatures"]["m_pStudioHdr"];
			signatures::m_yawClassPtr = update["signatures"]["m_yawClassPtr"];
			signatures::m_pitchClassPtr = update["signatures"]["m_pitchClassPtr"];
			signatures::interface_engine_cvar = update["signatures"]["interface_engine_cvar"];
			signatures::convar_name_hash_table = update["signatures"]["convar_name_hash_table"];
			signatures::m_bDormant = update["signatures"]["m_bDormant"];
			signatures::model_ambient_min = update["signatures"]["model_ambient_min"];
			signatures::set_abs_angles = update["signatures"]["set_abs_angles"];
			signatures::set_abs_origin = update["signatures"]["set_abs_origin"];
			signatures::is_c4_owner = update["signatures"]["is_c4_owner"];
			signatures::force_update_spectator_glow = update["signatures"]["force_update_spectator_glow"];
			signatures::anim_overlays = update["signatures"]["anim_overlays"];
			signatures::m_flSpawnTime = update["signatures"]["m_flSpawnTime"];
			signatures::find_hud_element = update["signatures"]["find_hud_element"];
			netvars::m_ArmorValue = update["netvars"]["m_ArmorValue"];
			netvars::m_Collision = update["netvars"]["m_Collision"];
			netvars::m_CollisionGroup = update["netvars"]["m_CollisionGroup"];
			netvars::m_Local = update["netvars"]["m_Local"];
			netvars::m_MoveType = update["netvars"]["m_MoveType"];
			netvars::m_OriginalOwnerXuidHigh = update["netvars"]["m_OriginalOwnerXuidHigh"];
			netvars::m_OriginalOwnerXuidLow = update["netvars"]["m_OriginalOwnerXuidLow"];
			netvars::m_aimPunchAngle = update["netvars"]["m_aimPunchAngle"];
			netvars::m_aimPunchAngleVel = update["netvars"]["m_aimPunchAngleVel"];
			netvars::m_bGunGameImmunity = update["netvars"]["m_bGunGameImmunity"];
			netvars::m_bHasDefuser = update["netvars"]["m_bHasDefuser"];
			netvars::m_bHasHelmet = update["netvars"]["m_bHasHelmet"];
			netvars::m_bInReload = update["netvars"]["m_bInReload"];
			netvars::m_bIsDefusing = update["netvars"]["m_bIsDefusing"];
			netvars::m_bIsScoped = update["netvars"]["m_bIsScoped"];
			netvars::m_bSpotted = update["netvars"]["m_bSpotted"];
			netvars::m_bSpottedByMask = update["netvars"]["m_bSpottedByMask"];
			netvars::m_dwBoneMatrix = update["netvars"]["m_dwBoneMatrix"];
			netvars::m_fAccuracyPenalty = update["netvars"]["m_fAccuracyPenalty"];
			netvars::m_fFlags = update["netvars"]["m_fFlags"];
			netvars::m_hViewModel = update["netvars"]["m_hViewModel"];
			netvars::m_flFallbackWear = update["netvars"]["m_flFallbackWear"];
			netvars::m_flFlashDuration = update["netvars"]["m_flFlashDuration"];
			netvars::m_flFlashMaxAlpha = update["netvars"]["m_flFlashMaxAlpha"];
			netvars::m_flNextPrimaryAttack = update["netvars"]["m_flNextPrimaryAttack"];
			netvars::m_hActiveWeapon = update["netvars"]["m_hActiveWeapon"];
			netvars::m_hMyWeapons = update["netvars"]["m_hMyWeapons"];
			netvars::m_hObserverTarget = update["netvars"]["m_hObserverTarget"];
			netvars::m_hOwner = update["netvars"]["m_hOwner"];
			netvars::m_hOwnerEntity = update["netvars"]["m_hOwnerEntity"];
			netvars::m_iAccountID = update["netvars"]["m_iAccountID"];
			netvars::m_iClip1 = update["netvars"]["m_iClip1"];
			netvars::m_zoomLevel = update["netvars"]["m_zoomLevel"];
			netvars::m_iCompetitiveRanking = update["netvars"]["m_iCompetitiveRanking"];
			netvars::m_iCompetitiveWins = update["netvars"]["m_iCompetitiveWins"];
			netvars::m_iCrosshairId = update["netvars"]["m_iCrosshairId"];
			netvars::m_iEntityQuality = update["netvars"]["m_iEntityQuality"];
			netvars::m_iFOVStart = update["netvars"]["m_iFOVStart"];
			netvars::m_iGlowIndex = update["netvars"]["m_iGlowIndex"];
			netvars::m_iHealth = update["netvars"]["m_iHealth"];
			netvars::m_iItemDefinitionIndex = update["netvars"]["m_iItemDefinitionIndex"];
			netvars::m_iItemIDHigh = update["netvars"]["m_iItemIDHigh"];
			netvars::m_iObserverMode = update["netvars"]["m_iObserverMode"];
			netvars::m_iShotsFired = update["netvars"]["m_iShotsFired"];
			netvars::m_iState = update["netvars"]["m_iState"];
			netvars::m_iTeamNum = update["netvars"]["m_iTeamNum"];
			netvars::m_lifeState = update["netvars"]["m_lifeState"];
			netvars::m_nFallbackPaintKit = update["netvars"]["m_nFallbackPaintKit"];
			netvars::m_nFallbackSeed = update["netvars"]["m_nFallbackSeed"];
			netvars::m_nFallbackStatTrak = update["netvars"]["m_nFallbackStatTrak"];
			netvars::m_iDefaultFOV = update["netvars"]["m_iDefaultFOV"];
			netvars::m_iFOV = update["netvars"]["m_iFOV"];
			netvars::m_nForceBone = update["netvars"]["m_nForceBone"];
			netvars::m_nTickBase = update["netvars"]["m_nTickBase"];
			netvars::m_rgflCoordinateFrame = update["netvars"]["m_rgflCoordinateFrame"];
			netvars::m_szCustomName = update["netvars"]["m_szCustomName"];
			netvars::m_szLastPlaceName = update["netvars"]["m_szLastPlaceName"];
			netvars::m_vecOrigin = update["netvars"]["m_vecOrigin"];
			netvars::m_vecVelocity = update["netvars"]["m_vecVelocity"];
			netvars::m_vecViewOffset = update["netvars"]["m_vecViewOffset"];
			netvars::m_viewPunchAngle = update["netvars"]["m_viewPunchAngle"];
			netvars::m_thirdPersonViewAngles = update["netvars"]["m_thirdPersonViewAngles"];
			netvars::m_clrRender = update["netvars"]["m_clrRender"];
			netvars::m_bBombTicking = update["netvars"]["m_bBombTicking"];
			netvars::m_bBombDefused = update["netvars"]["m_bBombDefused"];
			netvars::m_flC4Blow = update["netvars"]["m_flC4Blow"];
			netvars::m_hBombDefuser = update["netvars"]["m_hBombDefuser"];
			netvars::m_flTimerLength = update["netvars"]["m_flTimerLength"];
			netvars::m_flDefuseLength = update["netvars"]["m_flDefuseLength"];
			netvars::m_flDefuseCountDown = update["netvars"]["m_flDefuseCountDown"];
			netvars::m_nBombSite = update["netvars"]["m_nBombSite"];
			netvars::cs_gamerules_data = update["netvars"]["cs_gamerules_data"];
			netvars::m_SurvivalRules = update["netvars"]["m_SurvivalRules"];
			netvars::m_SurvivalGameRuleDecisionTypes = update["netvars"]["m_SurvivalGameRuleDecisionTypes"];
			netvars::m_bIsValveDS = update["netvars"]["m_bIsValveDS"];
			netvars::m_bFreezePeriod = update["netvars"]["m_bFreezePeriod"];
			netvars::m_bBombPlanted = update["netvars"]["m_bBombPlanted"];
			netvars::m_bIsQueuedMatchmaking = update["netvars"]["m_bIsQueuedMatchmaking"];
			netvars::m_flSimulationTime = update["netvars"]["m_flSimulationTime"];
			netvars::m_flLowerBodyYawTarget = update["netvars"]["m_flLowerBodyYawTarget"];
			netvars::m_angEyeAnglesX = update["netvars"]["m_angEyeAnglesX"];
			netvars::m_angEyeAnglesY = update["netvars"]["m_angEyeAnglesY"];
			netvars::m_flNextAttack = update["netvars"]["m_flNextAttack"];
			netvars::m_iMostRecentModelBoneCounter = update["netvars"]["m_iMostRecentModelBoneCounter"];
			netvars::m_flLastBoneSetupTime = update["netvars"]["m_flLastBoneSetupTime"];
			netvars::m_bStartedArming = update["netvars"]["m_bStartedArming"];
			netvars::m_bUseCustomBloomScale = update["netvars"]["m_bUseCustomBloomScale"];
			netvars::m_bUseCustomAutoExposureMin = update["netvars"]["m_bUseCustomAutoExposureMin"];
			netvars::m_bUseCustomAutoExposureMax = update["netvars"]["m_bUseCustomAutoExposureMax"];
			netvars::m_flCustomBloomScale = update["netvars"]["m_flCustomBloomScale"];
			netvars::m_flCustomAutoExposureMin = update["netvars"]["m_flCustomAutoExposureMin"];
			netvars::m_flCustomAutoExposureMax = update["netvars"]["m_flCustomAutoExposureMax"];
			netvars::m_nViewModelIndex = update["netvars"]["m_nViewModelIndex"];
		}


		Hitbox[0].Setup(BONE_PELVIS, Vector3D{ -6.42f, -5.7459f, -6.8587f }, Vector3D{4.5796f, 4.5796f, 6.8373f});
		Hitbox[1].Setup(BONE_L_THIGH, Vector3D{1.819f, -3.959f, -2.14f}, Vector3D{22.149002f, 3.424f, 4.5796f});
		Hitbox[2].Setup(BONE_L_CALF, Vector3D{2.0758f, -3.21f, -2.1507f}, Vector3D{19.26f, 2.675f, 3.0495f});
		Hitbox[3].Setup(BONE_L_FOOT, Vector3D{1.8725f, -2.675f, -2.4075f}, Vector3D{5.6175f, 9.694201f, 2.4075f});
		Hitbox[4].Setup(BONE_R_THIGH, Vector3D{1.819f, -3.7557f, -4.5796f}, Vector3D{22.149002f, 3.424f, 2.14f});
		Hitbox[5].Setup(BONE_R_CALF, Vector3D{2.0758f, -3.21f, -2.8462f}, Vector3D{19.26f, 2.675f, 2.247f});
		Hitbox[6].Setup(BONE_R_FOOT, Vector3D{1.8725f, -2.675f, -2.4075f}, Vector3D{5.6175f, 9.694201, 2.4075f});
		Hitbox[7].Setup(BONE_SPINE2, Vector3D{-4.28f, -4.5796f, -6.3879f}, Vector3D{3.21f, 5.885f, 6.2809f});
		Hitbox[8].Setup(BONE_SPINE3, Vector3D{-4.28f, -5.029f, -6.0883f}, Vector3D{3.21f, 5.885f, 5.9813f});
		Hitbox[9].Setup(BONE_SPINE4, Vector3D{-4.28f, -5.35f, -5.885f}, Vector3D{2.9211f, 5.1467f, 5.885f});
		Hitbox[10].Setup(BONE_NECK, Vector3D{0.3317f, -3.0174f, -2.4503f}, Vector3D{3.4026f, 2.4182f, 2.354f});
		Hitbox[11].Setup(BONE_HEAD, Vector3D{-2.7713f, -2.8783f, -3.103f}, Vector3D{6.955f, 3.5203f, 3.0067f});
		Hitbox[12].Setup(BONE_L_UPPER_ARM, Vector3D{-2.675f, -3.21f, -2.14f}, Vector3D{12.84f, 3.21f, 2.14f});
		Hitbox[13].Setup(BONE_L_FOREARM, Vector3D{-0.f, -2.14f, -2.14f}, Vector3D{9.63f, 2.14f, 2.14f});
		Hitbox[14].Setup(BONE_L_HAND, Vector3D{-1.7227f, -1.2198f, -1.3803f}, Vector3D{4.4726f, 1.2198f, 1.3803f});
		Hitbox[15].Setup(BONE_R_UPPER_ARM, Vector3D{-2.675f, -3.21f, -2.14f}, Vector3D{12.84f, 3.21f, 2.14f});
		Hitbox[16].Setup(BONE_R_FOREARM, Vector3D{-0.f, -2.14f, -2.14f}, Vector3D{9.63f, 2.14f, 2.14f});
		Hitbox[17].Setup(BONE_R_HAND, Vector3D{-1.7227f, -1.2198f, -1.3803f}, Vector3D{4.4726f, 1.2198f, 1.3803f});
		Hitbox[18].Setup(BONE_L_CLAVICLE, Vector3D{-0.f, -3.21f, -5.35f}, Vector3D{7.49f, 4.28f, 3.21f});
		Hitbox[19].Setup(BONE_R_CLAVICLE, Vector3D{-0.f, -3.21f, -3.21f}, Vector3D{7.49f, 4.28f, 5.35f});
		Hitbox[20].Setup(BONE_HEAD, Vector3D{-2.5038f, 2.009f, -1.1021f}, Vector3D{6.3023f, 5.2965f, 0.9951f});
		Hitbox[21].Setup(BONE_SPINE4, Vector3D{-0.2996f, -6.0027f, -4.996901f}, Vector3D{5.4998f, 2.5038f, 5.1039f});
	}
}

	void UpdateOffest(int &value, DWORD Module, char *pattern, char *sig, int offest, int extra)
	{
		value = PointerScanPatternEx(phandle, Module, SIZE_, pattern, sig, offest, extra);
	};

	struct sigmask
	{
		char *sig;
		char *mask;
		std::string strsig;
		std::string strmask;
	};

	sigmask chonvertIDAtoADB(std::string pattern)
	{
		sigmask ret;
		while (pattern.find("?") != std::string::npos)
			pattern.replace(pattern.find("?"), 1, "00");
		while (pattern.find(" ") != std::string::npos)
			pattern.replace(pattern.find(" "), 1, "\\x");
		pattern.insert(0, "\\x");

		std::string mask = "";

		for (size_t i = 1; i <= pattern.length() / 4; i++)
		{
			int32_t index = i * 4 - 1;

			if (pattern.at(index) == '0' && pattern.at(index - 1) == '0')
				mask += "?";
			else
				mask += "x";
		}

		//std::cout << "Code Style Mask: " << mask << '\n';

		//std::cout << "Code Style Pattern: " << pattern << '\n';
		//ret.mask = &mask[0];
		ret.sig = &pattern[0];
		std::copy(mask.begin(), mask.end(), ret.mask);
		ret.strmask = mask;
		ret.strsig = pattern;

		return ret;
	}

	void update_patttern ()
	{
		std::ifstream file("config.json");
		json update = json::parse(file);
		std::cout << update["signatures"].size() << "\n";

		for (size_t i = 0; i < update["signatures"].size(); i++)
		{
	    std::cout << update["signatures"][i]["module"].get<std::string>() << "\n";
		std::cout << update["signatures"][i]["name"].get<std::string>() << "\n";
		std::cout << update["signatures"][i]["pattern"].get<std::string>() << "\n";
		std::cout << update["signatures"][i]["extra"] << "\n";
		std::cout << update["signatures"][i]["offsets"][0] << "\n";

		update["signatures"][i]["name"].get<std::string>();
		//UpdateOffest(dwLocalPlayer,Client,"","",0,0);
		}
	}

	void update_patttern_to_file()
	{
		std::ifstream file("config.json");
		json update = json::parse(file);
		std::cout << update["signatures"].size() << "\n";

		std::ofstream schreiben;
		std::ifstream FileTest("newpattern.txt");
		if (!FileTest.is_open())
		{
		schreiben.open("newpattern.txt");
		for (size_t i = 0; i < update["signatures"].size(); i++)
		{
			sigmask test = chonvertIDAtoADB(update["signatures"][i]["pattern"].get<std::string>());
			/*std::cout << "Code Style Mask: " << test.mask << '\n';

			std::cout << "Code Style Pattern: " << test.sig << '\n';

			std::cout << update["signatures"][i]["module"].get<std::string>() << "\n";
			std::cout << update["signatures"][i]["name"].get<std::string>() << "\n";
			std::cout << update["signatures"][i]["pattern"].get<std::string>() << "\n";
			std::cout << update["signatures"][i]["extra"] << "\n";
			std::cout << update["signatures"][i]["offsets"][0] << "\n";

			update["signatures"][i]["name"].get<std::string>();
			UpdateOffest(dwLocalPlayer, Client, "", "", 0, 0);*/


				std::string module;
				std::string extra;
				std::string offsets;

				if (update["signatures"][i]["module"].get<std::string>() == "client.dll")
				{
					module = "Client";
				}
				else
				{
					if (update["signatures"][i]["module"].get<std::string>() == "engine.dll")
					{
						module = "Engine";
					}
				}

				if (update["signatures"][i]["extra"].is_null() == true)
				{
					extra = "0";
				}
				else
				{
					extra = std::to_string(update["signatures"][i]["extra"].get<int>());
				}
				//std::cout << update["signatures"][i]["extra"] << "\n";
				//std::cout << update["signatures"][i]["offsets"][0] << "\n";
				
				//Bug WTF
				/*if (update["signatures"][i]["offsets"][0] == 0)
				{
					offsets = "0";
				}
				else
				{
				   update["signatures"][i]["offsets"][0].get_allocator();
				}*/
		
				//schreiben << "int " << update["signatures"][i]["name"].get<std::string>() << " = 0;" << "\n";
				schreiben << update["signatures"][i]["name"].get<std::string>() << " = PointerScanPatternEx(phandle, " << module << ", SIZE_, " << '"' << test.strsig << '"' << ", " << '"' << test.strmask << '"' << ", " << update["signatures"][i]["offsets"][0] << ", " << extra << ");" << "\n";
				//value = PointerScanPatternEx(phandle, Module, SIZE_, pattern, sig, offest, extra);
			}
		}
		for (size_t i = 0; i < update["signatures"].size(); i++)
		{
			schreiben << "signatures::" + update["signatures"][i]["name"].get<std::string>() + " = update[" + '"' + "signatures" + '"' + "][" + '"' + update["signatures"][i]["name"].get<std::string>() + '"' + "]; ";
		}

		for (size_t i = 0; i < update["netvars"].size(); i++)
		{
			schreiben << "netvars::" + update["netvars"][i]["name"].get<std::string>() + " = update[" + '"' + "netvars" + '"' + "][" + '"' + update["netvars"][i]["name"].get<std::string>() + '"' + "]; ";
		}
		schreiben.close();
	}

	void update_pointer_to_file()
	{
		std::ifstream file("config.json");


		json update = json::parse(file);
		std::cout << update["signatures"].size() << "\n";

		std::ofstream schreiben;
		std::ifstream FileTest("newpattern.txt");
		if (!FileTest.is_open())
		{
			schreiben.open("newpattern.txt");
		}
		for (size_t i = 0; i < update["signatures"].size(); i++)
		{
			schreiben << "signatures::" + update["signatures"][i]["name"].get<std::string>() + " = update[" + '"' + "signatures" + '"' + "][" + '"' + update["signatures"][i]["name"].get<std::string>() + '"' + "]; " << "\n";
		}

		for (size_t i = 0; i < update["netvars"].size(); i++)
		{
			schreiben << "netvars::" + update["netvars"][i]["name"].get<std::string>() + " = update[" + '"' + "netvars" + '"' + "][" + '"' + update["netvars"][i]["name"].get<std::string>() + '"' + "]; " << "\n";
		}
		schreiben.close();
	}

	void readpointerconfig()
	{
		std::ifstream file("csgo.json");
		json update = json::parse(file);
		std::cout << update["signatures"].size() << "\n";
		
		for (size_t i = 0; i < update["signatures"].size(); i++)
		{
			if (update["signatures"][i].is_null() == true)
			{
				std::cout << update["signatures"][i].get<int>() << std::endl;
			}
			std::cout << update["signatures"] << std::endl;
		}
	}

	void ScriptLoad()
	{
		/*std::ifstream file("csgo.json");
		json update = json::parse(file);
		std::cout << update["signatures"].size() << "\n";
		for (size_t i = 0; i < update["signatures"].size(); i++)
		{
			//std::cout << update["signatures"][i] << "\n";
		}
		std::cout << update["signatures"]["dwLocalPlayer"] << "\n";*/
		//update_patttern_to_file();
		//update_patttern();
		//std::cout << "Debug: " << IsAccuracyPenalty() << "\n";
		//std::cout << std::hex << "Debug: " << Client << "\n";
		//std::cout << std::hex << "Debug: " << SIZE_ << "\n";
		//HMODULE DLL = LoadLibrary(_T("kernel32.dll"));
		//std::cout << phandle << std::endl;
		//https://github.com/A5-/CSGO-External/blob/master/csgo-extern/globals.h
		//https://www.unknowncheats.me/forum/counterstrike-source/192293-perfect-nospread.html
		//https://www.unknowncheats.me/forum/counterstrike-global-offensive/234466-spread-seed-prediction-nospread-fix.html
		/*float ServerTime = ReadMemory<float>(phandle, GetdwLocalPlayer() + netvars::m_nTickBase);
		std::cout << "Getm_flNextPrimaryAttack: "<<Getm_flNextPrimaryAttack() << "\n";
		std::cout << "GlobalVars :" <<  GlobalVars() << "\n";
		std::cout << "ServerTime +  GlobalVars :"<< ServerTime * GlobalVars() << "\n";
		Sleep(1000);*/
		//std::cout << Getm_flNextPrimaryAttack() << "\n";
		//std::cout << IsAbleToShoot() << "\n";
		//std::cout << GetSensitivityVal() << "\n";
		//SetSensitivityVal(1);
		Sleep(1000);

	}

