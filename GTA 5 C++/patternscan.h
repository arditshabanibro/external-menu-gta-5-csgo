#pragma once
#include "stdafx.h"
#include <Windows.h>
#include <TlHelp32.h>
#include "Memory.h"

//External Pattern Scan
DWORD_PTR ScanPatternEx(HANDLE hProc, DWORD_PTR base, DWORD len, char* sig, char* mask);
DWORD_PTR PointerScanPatternEx(HANDLE hProc, DWORD_PTR base, DWORD len, char* sig, char* mask);