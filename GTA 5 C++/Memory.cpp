#pragma once
#include "Memory.h"
#include "Menu.h"
#include "Hack.h"
#include <conio.h>
#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <vector>
#include <TlHelp32.h>
extern class vector3;
extern DWORD SIZE_;

int GetModulBaseAddr(LPCWSTR ProcessName, LPCWSTR ModuleName)
{
	HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 PE32;

	if (HSnap == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	PE32.dwSize = sizeof(PROCESSENTRY32);
	if (Process32First(HSnap, &PE32) == 0)
	{
		CloseHandle(HSnap);
		return 0;
	}

	do
	{
		if (lstrcmp(PE32.szExeFile, ProcessName) == 0)
		{
			int PID;
			PID = PE32.th32ProcessID;

			HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, PID);
			MODULEENTRY32 xModule;

			if (HSnap == INVALID_HANDLE_VALUE)
			{
				return 0;
			}
			xModule.dwSize = sizeof(MODULEENTRY32);
			if (Module32First(HSnap, &xModule) == 0)
			{
				CloseHandle(HSnap);
				return 0;
			}

			do
			{
				if (lstrcmp(xModule.szModule, ModuleName) == 0)
				{
					CloseHandle(HSnap);
					SIZE_ = xModule.modBaseSize;
					return (__int64)xModule.modBaseAddr;
				}
			} while (Module32Next(HSnap, &xModule));
			CloseHandle(HSnap);
			return 0;
		}
	} while (Process32Next(HSnap, &PE32));
	CloseHandle(HSnap);
	return 0;


}

DWORD64 GetModulBaseAddr64(LPCWSTR ProcessName, LPCWSTR ModuleName)
{
	HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 PE32;

	if (HSnap == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	PE32.dwSize = sizeof(PROCESSENTRY32);
	if (Process32First(HSnap, &PE32) == 0)
	{
		CloseHandle(HSnap);
		return 0;
	}

	do
	{
		if (lstrcmp(PE32.szExeFile, ProcessName) == 0)
		{
			int PID;
			PID = PE32.th32ProcessID;

			HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, PID);
			MODULEENTRY32 xModule;

			if (HSnap == INVALID_HANDLE_VALUE)
			{
				return 0;
			}
			xModule.dwSize = sizeof(MODULEENTRY32);
			if (Module32First(HSnap, &xModule) == 0)
			{
				CloseHandle(HSnap);
				return 0;
			}

			do
			{
				if (lstrcmp(xModule.szModule, ModuleName) == 0)
				{
					CloseHandle(HSnap);
					SIZE_ = xModule.modBaseSize;
					return (__int64)xModule.modBaseAddr;
				}
			} while (Module32Next(HSnap, &xModule));
			CloseHandle(HSnap);
			return 0;
		}
	} while (Process32Next(HSnap, &PE32));
	CloseHandle(HSnap);
	return 0;


}

/*DWORD64 MemoryRead(HANDLE phandle, DWORD64 adresse, DWORD64 size)
{
	ReadProcessMemory(phandle, (void*)adresse, &size, size, 0);
	return size;
}

void MemoryWrite(HANDLE phandle, DWORD64 adresse, DWORD64 value, DWORD64 size)
{
	WriteProcessMemory(phandle, (void*)adresse, &value, size, 0);
}

DWORD64 MemoryReadOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 size, std::vector<unsigned int> offsets)
{
	for (unsigned int i = 0; i < offsets.size(); ++i)
	{
		ReadProcessMemory(phandle, (void*)adresse, &adresse, sizeof(adresse), 0);
		adresse = adresse + offsets[i];
	}
	ReadProcessMemory(phandle, (void*)adresse, &adresse, size, 0);
	return adresse;
}

void MemoryWriteOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 value, DWORD64 size, std::vector<unsigned int> offsets)
{
	//TestInfo(24332432432);
	for (unsigned int i = 0; i < offsets.size(); ++i)
	{
		ReadProcessMemory(phandle, (void*)adresse, &adresse, sizeof(adresse), 0);
		adresse = adresse + offsets[i];
	}
	WriteProcessMemory(phandle, (void*)adresse, &value, size, 0);
}*/

/*#pragma once
#include "Memory.h"
#include "Menu.h"
#include "Hack.h"
#include <conio.h> 
#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <vector>
#include <TlHelp32.h>
extern DWORD SIZE_;

int GetModulBaseAddr(LPCWSTR ProcessName, LPCWSTR ModuleName)
{
	HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 PE32;

	if (HSnap == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	PE32.dwSize = sizeof(PROCESSENTRY32);
	if (Process32First(HSnap, &PE32) == 0)
	{
		CloseHandle(HSnap);
		return 0;
	}

	do
	{
		if (lstrcmp(PE32.szExeFile, ProcessName) == 0)
		{
			int PID;
			PID = PE32.th32ProcessID;

			HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, PID);
			MODULEENTRY32 xModule;

			if (HSnap == INVALID_HANDLE_VALUE)
			{
				return 0;
			}
			xModule.dwSize = sizeof(MODULEENTRY32);
			if (Module32First(HSnap, &xModule) == 0)
			{
				CloseHandle(HSnap);
				return 0;
			}

			do
			{
				if (lstrcmp(xModule.szModule, ModuleName) == 0)
				{
					CloseHandle(HSnap);
					SIZE_ = xModule.modBaseSize;
					return (__int64)xModule.modBaseAddr;
				}
			} while (Module32Next(HSnap, &xModule));
			CloseHandle(HSnap);
			return 0;
		}
	} while (Process32Next(HSnap, &PE32));
	CloseHandle(HSnap);
	return 0;


}

DWORD64 GetModulBaseAddr64(LPCWSTR ProcessName, LPCWSTR ModuleName)
{
	HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	PROCESSENTRY32 PE32;

	if (HSnap == INVALID_HANDLE_VALUE)
	{
		return 0;
	}
	PE32.dwSize = sizeof(PROCESSENTRY32);
	if (Process32First(HSnap, &PE32) == 0)
	{
		CloseHandle(HSnap);
		return 0;
	}

	do
	{
		if (lstrcmp(PE32.szExeFile, ProcessName) == 0)
		{
			int PID;
			PID = PE32.th32ProcessID;

			HANDLE HSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, PID);
			MODULEENTRY32 xModule;

			if (HSnap == INVALID_HANDLE_VALUE)
			{
				return 0;
			}
			xModule.dwSize = sizeof(MODULEENTRY32);
			if (Module32First(HSnap, &xModule) == 0)
			{
				CloseHandle(HSnap);
				return 0;
			}

			do
			{
				if (lstrcmp(xModule.szModule, ModuleName) == 0)
				{
					CloseHandle(HSnap);
					SIZE_ = xModule.modBaseSize;
					return (__int64)xModule.modBaseAddr;
				}
			} while (Module32Next(HSnap, &xModule));
			CloseHandle(HSnap);
			return 0;
		}
	} while (Process32Next(HSnap, &PE32));
	CloseHandle(HSnap);
	return 0;


}

DWORD64 MemoryRead(HANDLE phandle, DWORD64 adresse, DWORD64 value)
{
	ReadProcessMemory(phandle, (void*)adresse, &value, sizeof(value), 0);
	return value;
}

void MemoryWrite(HANDLE phandle, DWORD64 adresse, DWORD64 value)
{
	WriteProcessMemory(phandle, (void*)adresse, &value, sizeof(value), 0);
}

DWORD64 MemoryReadOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 value, std::vector<unsigned int> offsets)
{
	for (unsigned int i = 0; i < offsets.size(); ++i)
	{
		ReadProcessMemory(phandle, (void*)adresse, &adresse, sizeof(adresse), 0);
		adresse = adresse + offsets[i];
	}
	ReadProcessMemory(phandle, (void*)adresse, &adresse, sizeof(adresse), 0);
	return adresse;
}

void MemoryWriteOffsets(HANDLE phandle, DWORD64 adresse, DWORD64 value, std::vector<unsigned int> offsets)
{
	for (unsigned int i = 0; i < offsets.size(); ++i)
	{
		ReadProcessMemory(phandle, (void*)adresse, &adresse, sizeof(adresse), 0);
		adresse = adresse + offsets[i];
	}
	WriteProcessMemory(phandle, (void*)adresse, &value, sizeof(value), 0);
	//std::cout << std::hex << adresse <<"\n";
}

DWORD64 MemoryRead64(HANDLE phandle, DWORD64 adresse, DWORD64 value)
{
	ReadProcessMemory(phandle, (void*)adresse, &value, sizeof(value), 0);
	return value;
}


void MemoryWrite64(HANDLE phandle, DWORD64 adresse, DWORD64 value)
{
	WriteProcessMemory(phandle, (void*)adresse, &value, sizeof(value), 0);
}*/