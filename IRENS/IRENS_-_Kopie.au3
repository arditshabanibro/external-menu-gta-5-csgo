;#RequireAdmin
;~ #include <MemoryPointer.au3>
;~ #include "aadll - KopieTB.au3"
;~ #include "memory.au3"
;~ #include "YatoMemory.au3"
#include <Misc.au3>
#include <WinAPI.au3>
HotKeySet("{END}", "Terminate")
Func Terminate()
    Exit
 EndFunc   ;==>Terminate

;~ Edit
;~
Global $Vector2D = DllStructCreate("struct; float X; float Y; endstruct")
Global $Vector3D = DllStructCreate("struct; float X; float Y; float Z;endstruct")
Global $BoneMatrix = DllStructCreate("struct; float junk01[3]; float X; float junk02[3]; float Y; float junk03[3]; float Z; endstruct")
Global $ViewMatrix = DllStructCreate("struct; float M11; float M12; float M13; float M14; float M21; float M22; float M23; float M24; float M31; float M32; float M33; float M34; float M41; float M42; float M43; float M44; endstruct")


$aimPointX = 0
$aimPointY = 0

Func AB()
if $AB = 1 Then
$AB = 0
Else
$AB = 1
EndIf
EndFunc

Local $hDLL = DllOpen("user32.dll")

$ClientDll = 0

AimbotLoop()

Func AimbotLoop()
	While True
Sleep(1)
	 ; If Not _IsPressed(37, $hDLL)  Then
		;	ContinueLoop
	  ;EndIf

$aCoord = PixelSearch(321, 153, 1588, 853,0xFFFF4D,0)

If Not @error Then
   $w2sHitbox = DllStructCreate("struct; float X; float Y; endstruct")
	 $w2sHitbox.X = $aCoord[0]
	 $w2sHitbox.Y = $aCoord[1]
	  $aimPoint = ScreenToDelta($w2sHitbox)
	  ;_WinAPI_Mouse_Event($MOUSEEVENTF_MOVE, $aimPoint.X, $aimPoint.Y)
	  MouseMove($aCoord[0], $aCoord[1])

	  ;$aimPoint.X/10
	  ;$aimPoint.Y/10

;~ 	  if $aimPoint.X and $aimPoint.Y < 1 Then
;~ 	  MouseDown($MOUSE_CLICK_LEFT) ; Set the left mouse button state s down.
;~ 	  Else
;~ 	  MouseUp($MOUSE_CLICK_LEFT) ; Set the left mouse button state as up.
;~ 	  EndIf
	  EndIf
	WEnd
EndFunc

Func ScreenToDelta($from)
	$ScreenCenterX =    @desktopWidth / 2
	$ScreenCenterY =    @desktopHeight / 2
	$TargetX = 0;
	$TargetY = 0;
	If $from.X <> 0 Then
		If $from.X > $ScreenCenterX Then
			$TargetX = -($ScreenCenterX - $from.X)
			If $TargetX + $ScreenCenterX > $ScreenCenterX * 2 Then
				$TargetX = 0
			Endif
		EndIf

		If $from.X < $ScreenCenterX Then
			$TargetX = $from.X - $ScreenCenterX;
			If $TargetX + $ScreenCenterX < 0 Then
				$TargetX = 0;
			EndIf
		Endif
	EndIf

	If $from.Y <> 0 Then
		If $from.Y > $ScreenCenterY Then
			$TargetY = -($ScreenCenterY - $from.Y)
			If $TargetY + $ScreenCenterY > $ScreenCenterY * 2 Then
				$TargetY = 0
			EndIf
		EndIf

		If $from.Y < $ScreenCenterY Then
			$TargetY = $from.Y - $ScreenCenterY
			If $TargetY + $ScreenCenterY < 0 Then
				$TargetY = 0
			EndIf
		EndIf
	EndIf

	$to = DllStructCreate("struct; float X; float Y; endstruct")
	$to.X = $TargetX
	$to.Y = $TargetY
	Return $to
EndFunc

Func DistanceBetweenCross($x, $y)
	$ydist = $y -    @desktopHeight / 2
	$xdist = $x -    @desktopWidth / 2
	Return Sqrt(($ydist ^ 2) + ($xdist ^ 2))
EndFunc